## Домашняя работа №1 
### Configure Router-on-a-Stick Inter-VLAN Routing

### Задание
1. Создание сети и настройка основных параметров устройств
2. Создание VLAN и назначение портов
3. Настройка транка между коммутаторами
4. Настройка маршрутизации между VLAN
5. Проверка работоспособности стенда

### Решение

##### Таблица сетевых адресов

| Device | Interface | IP Address | Subnet Mask | Default Gateway|
| --------|-----------|-------------|----------------|-------------------|
| R1 | e0/1.3 | 	192.168.3.1	| 255.255.255.0	| N/A |
|  | e0/1.4 | 	192.168.4.1	| 255.255.255.0 |	N/A |
| | e0/1.8 |	N/A	 | N/A |	N/A |
| S1|	 VLAN 3	| 192.168.3.11	| 255.255.255.0 |	192.168.3.1 |
| S2|	VLAN 3	| 192.168.3.12 | 255.255.255.0 | 192.168.3.1 |
|PC-A|	NIC	|192.168.3.3|	255.255.255.0	|192.168.3.1|
|PC-B|	NIC|	192.168.4.3|	255.255.255.0	|192.168.4.1|

##### Таблица VLAN

| VLAN	| NAME	| Interface Assigned |
| --------|-----------|-------------|
|3	| Management  |	S1: VLAN 3|
| | | S2: VLAN 3|
| | |S1: e1/0 |
|4| Operations | S2: e2/0 |
|7 | ParkingLot	| S1: e0/0-3,e1/0-3,e2/1-3,e3/0-3|
| | |S2: e0/0, e0/2-3,e1/0-3,e2/0-3,e3/1-3|
|8|	Native	| N/A |

Топология сети представлена на рисунке ниже:


![Схема взаимодействия](Labs/01_VLAN/scheme.jpg "Схема взаимодействия")



### 1 Создание сети и настройка основных параметров устройств

Первым шагом был настроен Router.

Задано имя устройству командой:

```
Router> enable
Router# config terminal
Router(config)#hostname R1
```

После отключения DNS были назначены пароли для входа в привилегированный режим, консоли и VTY.

```
R1(config)# no ip domain-lookup
R1(config)#enable secret router
R1(config)#line con 0
R1(config-line)#password eve
R1(config-line)#login
R1(config)#line vty 0 4
R1(config-line)#password eve
R1(config-line)#login
```
Пароли были зашифрованы в виде открытого текста и создан баннер, предупреждающий всех, кто обращается к устройству, о том, что несанкционированный доступ запрещен.

```
R1(config)# service password-encryption
R1(config)#banner m
R1(config)#banner motd `Log in! Only for authorized users`
```
Далее сохранена конфигурация и установлено время:

```
R1(config)# exit
R1# copy running-config startup-config
R1#clock set 00:25:00 25 September 2023
R1#*Sep 25 00:25:00.000: %SYS-6-CLOCKUPDATE: System clock has been updated from 21:26:24 UTC Sun Sep 24 2023 to 00:25:00 UTC Mon Sep 25 2023, configured from console by console.
```
Аналогичные настройки применены для устройств Switch (S4 и S5)

Заданы имена устройствам:

```
switch> enable
switch# config terminal
switch(config)# hostname S1
switch(config)# hostname S2
```
После отключения DNS были назначены пароли для входа в привилегированный режим, консоли и VTY. 
```
S1(config)# no ip domain-lookup
S2(config)# no ip domain-lookup
```
>Для удобства общие комнады  приводятся сразу для обоих устройств

```
S1(config)# enable secret switch
S2(config)# enable secret switch
```
```
S1(config)# line console 0
S1(config-line)# password eve
S1(config-line)# login

S2(config)# line console 0
S2(config-line)# password eve
S2(config-line)# login


S1(config)# line vty 0 4
S1(config-line)# password eve
S1(config-line)# login

S2(config)# line vty 0 4
S2(config-line)# password eve
S2(config-line)# login

```

Далее зашифрованы пароли и прописаны баннеры:
```
S1(config)# service password-encryption
S2(config)# service password-encryption

S1(config)# banner motd `Log in! Only for authorized users`
S2(config)# banner motd `Log in! Only for authorized users`

S1# clock set 00:31:00 25 September 2023
S2# clock set 00:37:00 25 September 2023
```

Также на этом шаге были заданы адреса усройствам РС в соответсвии с адресами, указанными в таблице.

```
VPCS> set pcname PC1

PC1> ip 192.168.3.3 255.255.255.0 192.168.3.1
Checking for duplicate address...
PC1 : 192.168.3.3 255.255.255.0 gateway 192.168.3.1

```
```
VPCS> set pcname PC2

PC2> ip 192.168.4.3 255.255.255.0 192.168.4.1
Checking for duplicate address...
PC2 : 192.168.4.3 255.255.255.0 gateway 192.168.4.1

```


### 2 Создание VLAN и назначение портов

На данном этапе необходимо создадать сети VLAN, в соответсвии с таблицей VLAN на обоих коммутаторах и назначить их соответствующим интерфейсам. 

Создание VLAN и назначение имен по таблице:
```
S1(config)# vlan 3
S1(config-vlan)# name Management
S1(config-vlan)# vlan 4
S1(config-vlan)# name Operations
S1(config-vlan)# vlan 7
S1(config-vlan)# name ParkingLot
S1(config-vlan)# vlan 8
S1(config-vlan)# name Native

S2(config)# vlan 3
S2(config-vlan)# name Management
S2(config-vlan)# vlan 4
S2(config-vlan)# name Operations
S2(config-vlan)# vlan 7
S2(config-vlan)# name ParkingLot
S1(config-vlan)# vlan 8
S1(config-vlan)# name Native
```
Настройка адресов и шлюза по умолчанию на каждом коммутаторе по таблице адресации:

```
S1(config)# interface vlan 3
S1(config-if)# ip address 192.168.3.11 255.255.255.0
S1(config-if)# no shutdown
S1(config-if)# exit
S1(config)# ip default-gateway 192.168.3.1

S2(config)# interface vlan 3
S2(config-if)# ip address 192.168.3.12 255.255.255.0
S2(config-if)# no shutdown
S2(config-if)# exit
S2(config)# ip default-gateway 192.168.3.1

```

Назначение VLAN на неиспользуемые порты:

```
S1(config)# interface range et0/0-3, et1/0-3, et2/0-3, et3/0-3 
S1(config-if-range)# switchport mode access
S1(config-if-range)# switchport access vlan 7
S1(config-if-range)# shutdown

S2(config)# interface range et0/0-3, et1/0-3, et2/0-3, et3/0-3 
S2(config-if-range)# switchport mode access
S2(config-if-range)# switchport access vlan 7
S2(config-if-range)# shutdown

```

Далее назначены используемые порты соответствующей VLAN и приведены в режим доступа.

```
S1(config)# interface et1/0
S1(config-if)# switchport mode access
S1(config-if)# switchport access vlan 3

S2(config)# interface et2/0
S2(config-if)# switchport mode access
S2(config-if)# switchport access vlan 4
```
С помощью команды `show vlan brief` нужно проверить правильность назначения портов: 

```
S1# show vlan brief
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    
3    Management                       active    Et1/0
4    Operations                       active
7    ParkingLot                       active    Et0/0, Et0/1, Et0/2, Et0/3
                                                Et1/1, Et1/2, Et1/3, Et2/0
                                                Et2/1, Et2/2, Et2/3, Et3/0
                                                Et3/1, Et3/2, Et3/3
                                                
8    Native                           active
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 
```
Аналогично для S2.

```
S2# show vlan brief
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    
3    Management                       active    
4    Operations                       active    Et2/0
7    ParkingLot                       active    Et0/0, Et0/1, Et0/2, Et0/3
                                                Et1/0, Et1/1, Et1/2, Et1/3 
                                                Et2/1, Et2/2, Et2/3, Et3/0
                                                Et3/1, Et3/2, Et3/3
                                                
8    Native                           active
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 
```
### 3 Настройка транка между коммутаторами

На данном шаге необходимо настроить интерфейс e0/1 в качестве транка.

Для этого на обоих устройствах выполнены следующие команды.

Изменение режима порта коммутатора на интерфейсе е0/1, чтобы принудительно создать транкинг.

```
S1(config)# interface et0/1
S1(config-if)# switchport mode trunk
```

Настройка конфигураций: 

```
S1(config-if)# switchport trunk native vlan 8
S1(config-if)# switchport trunk allowed vlan 3,4,8
```
В качестве проверки настройки нужно посмотреть вывод команды `show interfaces trunk`:

```
S1#show int trunk 

Port        Mode             Encapsulation  Status        Native vlan
Et0/1       auto             802.1q         trunking      8

Port        Vlans allowed on trunk
Et0/1       3-4,8

Port        Vlans allowed and active in management domain
Et0/1       3-4,8

Port        Vlans in spanning tree forwarding state and not pruned
Et0/1       3-4,8
```
Аналогично предыдущему шагу ощуствлена настройка на интерфейсе e0/0 S1:

```
S1(config)#int et0/0
S1(config-if)#switchport trunk encapsulation dot1q
S1(config-if)#switchport mode trunk
S1(config-if)#switchport trunk native vlan 8
S1(config-if)#switchport trunk allowed vlan 3,4,8 

```
Проверка:

```
S1#show int trunk 

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      8
Et0/1       on               802.1q         trunking      8

Port        Vlans allowed on trunk
Et0/0       3-4,8
Et0/1       3-4,8

Port        Vlans allowed and active in management domain
Et0/0       3-4,8
Et0/1       3-4,8

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       3-4,8
Et0/1       3-4,8
```

### 4 Настройка маршрутизации между VLAN

На первом этапе нужно активировать интерфейс на Router:

```
R1(config)# interface et0/1
R1(config-if)# no shutdown
R1(config-if)# exit

```

Далее произведена настройка подинтерфейсов для каждой VLAN, как указано в таблице IP-адресации. Все подинтерфейсы используют инкапсуляцию 802.1Q. 

```
R1(config)# interface et0/1.3
R1(config-subif)# description Management Network
R1(config-subif)# encapsulation dot1q 3
R1(config-subif)# ip address 192.168.3.1 255.255.255.0
R1(config-subif)# interface et0/1.4
R1(config-subif)# description Operations Network
R1(config-subif)# encapsulation dot1q 4
R1(config-subif)# ip address 192.168.4.1 255.255.255.0
R1(config-subif)# interface et0/1.8
R1(config-subif)# description Native VLAN
R1(config-subif)# encapsulation dot1q 8 native 

```
Проверка: 

```
R1# show ip interface brief
Interface              IP-Address      OK? Method Status                Protocol
Ethernet0/0             unassigned      YES unset  up                    up
Ethernet0/1             unassigned      YES unset  up                    up
Ethernet0/1.3           192.168.3.1     YES manual up                    up
Ethernet0/1.4           192.168.4.1     YES manual up                    up
Ethernet0/1.8           unassigned      YES unset  up                    up
```

### 5 Проверка работоспособности стенда

```
PC-A> ping 192.168.3.1 // доступность шлюза по умолчанию

84 bytes from 192.168.3.1 icmp_seq=1 ttl=255 time=0.356 ms
84 bytes from 192.168.3.1 icmp_seq=2 ttl=255 time=0.472 ms
84 bytes from 192.168.3.1 icmp_seq=3 ttl=255 time=0.493 ms
84 bytes from 192.168.3.1 icmp_seq=4 ttl=255 time=0.510 ms
84 bytes from 192.168.3.1 icmp_seq=5 ttl=255 time=0.558 ms
```

```
PC-A> ping 192.168.4.3 // доступность PC-B

192.168.3.12 icmp_seq=1 timeout
84 bytes from 192.168.4.3 icmp_seq=2 ttl=255 time=1.007 ms
84 bytes from 192.168.4.3 icmp_seq=3 ttl=255 time=0.994 ms
84 bytes from 192.168.4.3 icmp_seq=4 ttl=255 time=0.893 ms
84 bytes from 192.168.4.3 icmp_seq=5 ttl=255 time=0.956 ms

PC-A> ping 192.168.3.12 // доступность S2

84 bytes from 192.168.3.12 icmp_seq=1 ttl=255 time=1.105 ms
84 bytes from 192.168.3.12 icmp_seq=2 ttl=255 time=0.683 ms
84 bytes from 192.168.3.12 icmp_seq=3 ttl=255 time=0.759 ms
84 bytes from 192.168.3.12 icmp_seq=4 ttl=255 time=0.639 ms
84 bytes from 192.168.3.12 icmp_seq=5 ttl=255 time=0.731 ms
```
Вывод трассировки показывает две записи в результатах. 
```
PC-B>trace 192.168.3.3
trace to 192.168.3.3, 8 hops max, press Ctrl+C to stop
 1   192.168.4.1   0.789 ms  0.548 ms  0.350 ms
 2   *192.168.3.3   0.685 ms (ICMP type:3, code:3, Destination port unreachable)
```


