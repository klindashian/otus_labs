## Домашнее задание 7
### IS-IS


### Цель:
1. Настроить IS-IS офисе Триада


### Описание/Пошаговая инструкция выполнения домашнего задания:
- Настроите IS-IS в ISP Триада.
- R23 и R25 находятся в зоне 2222.
- R24 находится в зоне 24.
- R26 находится в зоне 26.

### Настройка IS-IS в ISP Триада

![Схема взаимодействия](Labs/07_ISIS/lab6.png)

Для выполнения настроек необходимо на устройствах выполнить следующие шаги:

1. Включить IS-IS на маршрутизаторе: `router isis`/ `ipv6 router isis`
2. Настроить NET адрес для работы IS-IS:

``` 
R23 net 49.2222.0000.0000.0023.00
R24 net 49.2222.0000.0000.0025.00
R25 net 49.0024.0000.0000.0024.00
R26 net 49.0026.0000.0000.0026.00
```
3. Включить IS-IS на интерфейсах

Для примера приведена настройка R23:

``` 
R23(config)#router isis
R23(config-router)#net 49.2222.0000.0023.00
R23(config-router)#exit
R23(config)#int range et0/1-2
R23(config-if-range)#ip router isis
R23(config-if-range)#exit 
```

### Проверка работоспособности 

Все проверки будут выполнены также на маршрутизаторе R23:
Просмотр таблицы маршрутизации:

``` 
R23#sh ip route isis
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 8 subnets, 3 masks
i L2     10.10.2.20/30 [115/20] via 10.30.3.11, 00:52:51, Ethernet0/2
i L1     10.10.2.24/30 [115/20] via 10.30.3.2, 00:56:38, Ethernet0/1

```

```
R23#sh ipv6 route isis
IPv6 Routing Table - default - 11 entries
Codes: C - Connected, L - Local, S - Static, U - Per-user Static route
       B - BGP, HA - Home Agent, MR - Mobile Router, R - RIP
       H - NHRP, I1 - ISIS L1, I2 - ISIS L2, IA - ISIS interarea
       IS - ISIS summary, D - EIGRP, EX - EIGRP external, NM - NEMO
       ND - ND Default, NDp - ND Prefix, DCE - Destination, NDr - Redirect
       O - OSPF Intra, OI - OSPF Inter, OE1 - OSPF ext 1, OE2 - OSPF ext 2
       ON1 - OSPF NSSA ext 1, ON2 - OSPF NSSA ext 2, la - LISP alt
       lr - LISP site-registrations, ld - LISP dyn-eid, a - Application
I1  20FF:DB8:3000:2526::/64 [115/20]
     via FE80::25, Ethernet0/1
I2  2001:DB8:3000:2624::/64 [115/20]
     via FE80::24, Ethernet0/2
```

Просмотр соседей:

```
R23#sh isis neighbors

System Id      Type Interface   IP Address      State Holdtime Circuit Id
R24            L2   Et0/2       10.30.3.11      UP    8        R24.02           
R25            L1   Et0/1       10.30.3.2       UP    7        R25.02           
R25            L2   Et0/1       10.30.3.2       UP    9        R25.02
```