Building configuration...

Current configuration : 1131 bytes

!
version 15.4
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname R22
!
boot-start-marker
boot-end-marker
!
!
enable secret 5 68k.7dfegt/mkhg985.fds
!
no aaa new-model
clock timezone CST 3 0
mmi polling-interval 60
no mmi auto-configure
no mmi pvc
mmi snmp-timeout 180
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
no ip domain-lookup
ip cef
ipv6 unicast-routing
ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
cts logging verbose
!
!
!
redundancy
!
!
! 
!
!
!
!
!
!
!
!
!
!
!
!
!
interface Ethernet0/0
 no shutdown
 description to R14
 ip address 172.16.7.22 255.255.255.252
 ipv6 address FE80::22 link-local
 ipv6 address 2001:DB8:1000:2214::22/64
 ipv6 enable
!
interface Ethernet0/1
 no shutdown
 description to R21
 ip address 172.16.6.22 255.255.255.252
 ipv6 address FE80::22 link-local
 ipv6 address 2001:DB8:6000:2221::22/64
 ipv6 enable
!
interface Ethernet0/2
 no shutdown
 description to R23
 ip address 172.16.2.22 255.255.255.252
 ipv6 address FE80::22 link-local
 ipv6 address 2001:DB8:5000:2223::22/64
 ipv6 enable
!
interface Ethernet0/3
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/0
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/1
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/2
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/3
 no shutdown
 no ip address
 shutdown
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
!
!
!
control-plane
!
!
banner motd ^CAuthorized Users Only!^C
!
line con 0
 exec-timeout 0 0
 password 7 800HJ76F4D06
 logging synchronous
 login
line aux 0
line vty 0 4
 password 7 045F471A1A0A
 login
 transport input none
!
!
end
