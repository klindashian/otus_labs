Building configuration...

Current configuration : 1131 bytes
!
version 15.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname R19
!
boot-start-marker
boot-end-marker
!
!
boot-start-marker
boot-end-marker
!
enable secret 5 $0t9ER$ghj667edyT0ve
!
no aaa new-model
clock timezone CST 3 0
mmi polling-interval 60
no mmi auto-configure
no mmi pvc
mmi snmp-timeout 180
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
no ip domain lookup
ip cef
ipv6 unicast-routing
ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
cts logging verbose
!
!
!
redundancy
!
!
! 
!
!
!
!
!
!
!
!
!
!
!
!
interface Ethernet0/0
 no shutdown
 description to R14
 ip address 10.20.2.17 255.255.255.252
 ipv6 address FE80::19 link-local
 ipv6 address 2001:DB8:1000:1914::19/64
 ipv6 enable
!
interface Ethernet0/1
 no shutdown
 no ip address
 shutdown
!
interface Ethernet0/2
 no shutdown
 no ip address
 shutdown
!
interface Ethernet0/3
 no shutdown
 no ip address
 shutdown
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
!
!
!
control-plane
!
!
banner motd ^CAuthorized Users Only!^C
!
line con 0
 exec-timeout 0 0
 password 7 Y67816141D
 logging synchronous
 login
line aux 0
line vty 0 4
 password 7 IA000710754
 login
 transport input none
