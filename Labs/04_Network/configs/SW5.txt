Building configuration...

Current configuration : 1980 bytes
!
! Last configuration change at 11:33:22 EET Wed Apr 17 2024
!
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname SW5
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
clock timezone CST 3 0
!
!
!
!
!
!
!
!
no ip domain-lookup
ip cef
ipv6 unicast-routing
ipv6 cef
!
!
!
spanning-tree mode rapid-pvst
spanning-tree extend system-id
spanning-tree vlan 60 priority 24576
!
vlan internal allocation policy ascending
!
!
!
!
!
!
!
!
!
!
!
!
!
interface Loopback0
 ip address 60.60.60.60 255.255.255.255
!
interface Port-channel1
 description to SW4
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface Ethernet0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 channel-group 1 mode active
!
interface Ethernet0/3
 switchport trunk encapsulation dot1q
 switchport mode trunk
 channel-group 1 mode active
!
interface Ethernet1/0
 no switchport
 ip address 10.20.2.34 255.255.255.252
 duplex auto
 ipv6 address FE80::5 link-local
 ipv6 address 2001:db8:1000:513::5/64
!
interface Ethernet1/1
 no switchport
 ip address 10.20.2.2 255.255.255.252
 duplex auto
 ipv6 address FE80::5 link-local
 ipv6 address 2001:db8:1000:512::5/64
!
interface Ethernet1/2
 switchport access vlan 66
 switchport mode access
!
interface Ethernet1/3
!
interface Vlan50
 ip address 192.168.5.3 255.255.255.0
 standby 1 ip 192.168.5.50
 standby 1 preempt
!
interface Vlan60
 ip address 192.168.6.3 255.255.255.0
 standby 2 ip 192.168.6.60
 standby 2 priority 110
 standby 2 preempt
!
interface Vlan66
 ip address 192.168.66.66 255.255.255.0
!
ip forward-protocol nd
!
no ip http server
no ip http secure-server
!
!
!
!
!
!
control-plane
!
!
banner motd ^CAuthorized Users Only!^C
!
line con 0
 password 7 14141B180F0B
 logging synchronous
 login
line aux 0
line vty 0 4
 password 7 060506324F41
 login
!
!
end