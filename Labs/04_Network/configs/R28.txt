Building configuration...

Current configuration : 1675 bytes

!
version 15.4
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname R28
!
boot-start-marker
boot-end-marker
!
!
enable secret 5 $1$r3OH$oyjkIQ7jguT0vea.q678
!
no aaa new-model
clock timezone CST 3 0
mmi polling-interval 60
no mmi auto-configure
no mmi pvc
mmi snmp-timeout 180
!
!
!
!
!
!
!
no ip domain lookup
ip cef
ipv6 unicast-routing
ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
cts logging verbose
!
!
!
redundancy
!
!
! 
!
!
!
!
!
!
!
!
!
!
!
interface Ethernet0/0
 no shutdown
 description R28 to R26
 ip address 172.16.1.18 255.255.255.252
 ipv6 address FE80::28 link-local
 ipv6 address 2001:DB8:4000:2826::28/64
 ipv6 enable
!
interface Ethernet0/1
 no shutdown
 description R28 to R25
 ip address 172.16.1.29 255.255.255.252
 ipv6 address FE80::28 link-local
 ipv6 address 2001:DB8:4000:2825::28/64
 ipv6 enable
!
interface Ethernet0/2
 no shutdown
 no ip address
!
interface Ethernet0/2.10
 no shutdown
 description gate vlan10
 encapsulation dot1Q 10
 ip address 192.168.1.1 255.255.255.0
!
interface Ethernet0/2.20
 no shutdown
 description gate vlan20
 encapsulation dot1Q 20
 ip address 192.168.2.2 255.255.255.0
!
interface Ethernet0/2.33
 no shutdown
 description gate vlan33
 encapsulation dot1Q 33
 ip address 10.3.1.9 255.255.255.248
!
interface Ethernet0/3
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/0
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/1
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/2
 no shutdown
 no ip address
 shutdown
!
interface Ethernet1/3
 no shutdown
 no ip address
 shutdown
!
ip forward-protocol nd
!
!
no ip http server
no ip http secure-server
!
!
!
!
control-plane
!
banner motd ^CAuthorized Users Only!^C
!
line con 0
 password 7 050E100A
 logging synchronous
 login
line aux 0
line vty 0 4
 password 7 050E100A
 login
!
!
end
