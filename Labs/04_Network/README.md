## Домашнее задание 4
### Проектирование сети

### Цель:
1. В данной самостоятельной работе необходимо распланировать адресное пространство
2. Настроить IP на всех активных портах для дальнейшей работы над проектом
3. Адресное пространство должно быть задокументировано


### Описание/Пошаговая инструкция выполнения домашнего задания:

- Разработка и документация адресного пространства для лабораторного стенда.
- Настройка ip адреса на каждом активном порту
- Настройка каждый VPC в каждом офисе в своем VLAN.
- Настройка VLAN/Loopbackup interface управления для сетевых устройств
- Настройка сети офисов так, чтобы не возникало broadcast штормов, а использование линков было максимально оптимизировано

Топология сети представлена на рисунке ниже:

![Схема взаимодействия](Labs/04_Network/ip_1.jpg "Схема взаимодействия")

### Адресное пространство

Для формирования адресного пространства стенда были использованы следующие подходы:

* все адреса IPv4 относятся к сети 10.0.0.0/8 c разделением на подсети для каждого филиала

* для внешних адресов выделена сеть 172.16.0.0/16 c разделением на подсети для каждого филиала

* все локальные адреса IPv6 относятся к сети FE80::X/8, где Х - номер устройства

* все глобальные адреса IPv6 относятся к сети 2001:DB8::/48, и разделены.

Адреса IPv6 сформированы следующим образом - 2001:DB8:A000:CI:F/64, где:

    A - подсеть в соответсвии с рисунком
    C - номер устройства, где производится настройка 
    I - номер устройства, соединенного по интерфейсу с устройством С
    F - номер устройства, где производится настройка

Подробнее на схеме с указанием адресов:    

![Схема взаимодействия](Labs/04_Network/adr.jpg "Схема взаимодействия")

Таблица адресации:

*Чокурдах и Лабытнанги*

![Адреса](Labs/04_Network/ipv4_chok.png )

Для удобства перенесено в таблицу

**Чокурдах (ipv6 - 2001:db8:2000::/48) и Лабытнанги (ipv6 - 2001:db8:8000::/48)** 

| Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|------------|-----------|----------|----------------|----------------|
|  R28| e0/0 | 2001:db8:4000:2826::/64 | 2001:db8:4000:2826::28/64 | to e0/1 R26  |
|     | e0/1 | 2001:db8:4000:2825::/64 | 2001:db8:4000:2825::28/64 | to e0/3 R25  | 
|     | e0/2 | 2001:db8:4000:2829::/64 | 2001:db8:4000:2829::28/64 | to e0/2 SW29 |    
| R27 | e0/0 | 2001:db8:8000:2725::/64 | 2001:db8:2000:2725::27/64 | to e0/1 R25 |        
  
 
**Москва (ipv4 - 10.20.2.0/24)**

| Москва | Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|--------|------------|-----------|----------|----------------|----------------|
|        |  R12   | e0/0 | 10.20.2.24/30 | 10.20.2.25/30 | to e1/0 SW4  |
|        |    | e0/1 | 10.20.2.0/30 | 10.20.2.1/30 | to e1/1 SW5  | 
|        |        | e0/2 | 10.20.2.20/30  | 10.20.2.22/30  | to e0/0 R14  |   
|        |        | e0/3 | 10.20.2.8/30 | 10.20.2.10/30 | to e0/1 R15  |   
|        | R13 | e0/0 | 10.20.2.32/30 | 10.20.2.33/30 | to e1/0 SW5  |   
|        |   | e0/1 | 10.20.2.36/30 | 10.20.2.38/30 | to e1/1 SW4  |   
|        |   | e0/2 | 10.20.2.12/30 | 10.20.2.13/30 | to e0/0 R15  |   
|        |   | e0/3 | 10.20.2.28/30 | 10.20.2.30/30  | to e0/1 R14  |   
|        |   R14 | e0/0 | 10.20.2.20/30  | 10.20.2.21/30  | to e0/2 R12  |   
|        |         | e0/1 | 10.20.2.28/30  | 10.20.2.29/30  | to e0/1 R13  |   
|        |         | e0/2 | 172.16.7.20/30 | 172.16.7.21/30  | to e0/0 R22  |
|        |         | e0/3 | 10.20.2.16/30  | 10.20.2.18/30  | to e0/0 R19  |   
|        | R15 | e0/0 | 10.20.2.12/30 | 10.20.2.14/30 | to e0/2 R13  |   
|        |     | e0/1 | 10.20.2.8/30 | 10.20.2.9/30 | to e0/3 R12  |  
|        |     | e0/2 | 172.16.5.20/30 | 172.16.5.22/30 | to e0/0 R21  |
|        |     | e0/3 | 10.20.2.40/30 | 10.20.2.41/30 | to e0/0 R20  |   
|        |  R19 | e0/0 | 10.20.2.16/30  | 10.20.2.17/30  | to e0/3 R14  |   
|        | R20 | e0/0 | 10.20.2.40/30 | 10.20.2.42/30 | to e0/3 R21  |   
|        | SW4 | e1/0 | 10.20.2.24/30 | 10.20.2.26/30 | to e0/0 R12  |   
|        |     | e1/1 | 10.20.2.36/30 | 10.20.2.37/30 | to e0/1 R13  |   
|        | SW5 | e1/0 | 10.20.2.32/30 | 10.20.2.34/30 | to e0/0 R13  |  
|        |     | e1/1 | 10.20.2.0/30 | 10.20.2.2/30 | to e0/1 R12  |   
------------------------------------------------------------------------------

**Москва (ipv6 - 2001:db8:1000::/48)**


| Москва | Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|--------|------------|-----------|----------|----------------|----------------|
|        |  R12   | e0/0 | 2001:db8:1000:124::/64 | 2001:db8:1000:124::12/64 | to e1/0 SW4  |
|        |        | e0/1 | 2001:db8:1000:125::/64 | 2001:db8:1000:125::12/64 | to e1/1 SW5  | 
|        |        | e0/2 | 2001:db8:1000:1214::/64  | 2001:db8:1000:1214::12/64  | to e0/0 R14  |   
|        |        | e0/3 | 2001:db8:1000:1215::/64 | 2001:db8:1000:1215::12/64 | to e0/1 R15  |   
|        |    R13 | e0/0 | 2001:db8:1000:135::/64 | 2001:db8:1000:135::13/64 | to e1/0 SW5  |   
|        |        | e0/1 | 2001:db8:1000:134::/64 | 2001:db8:1000:134::13/64 | to e1/1 SW4  |   
|        |        | e0/2 | 2001:db8:1000:1315::/64 | 2001:db8:1000:1315::13/64 | to e0/0 R15  |   
|        |        | e0/3 | 2001:db8:1000:1314::/64 | 2001:db8:1000:1314::13/64 | to e0/1 R14  |   
|        |    R14 | e0/0 | 2001:db8:1000:1412::/64  | 2001:db8:1000:1412::14/64 | to e0/2 R12  |   
|        |        | e0/1 | 2001:db8:1000:1413::/64  | 2001:db8:1000:1413::14/64 | to e0/1 R13  |   
|        |        | e0/2 | 2001:db8:1000:1422::/64 | 2001:db8:1000:1422::14/64 | to e0/0 R22  |
|        |        | e0/3 | 2001:db8:1000:1419::/64  | 2001:db8:1000:1419::14/64 | to e0/0 R19  |   
|        | R15    | e0/0 | 2001:db8:1000:1513::/64 | 2001:db8:1000:1513::15/64| to e0/2 R13  |   
|        |        | e0/1 | 2001:db8:1000:1512::/64 | 2001:db8:1000:1512::15/64 | to e0/3 R12  |  
|        |        | e0/2 | 2001:db8:1000:1521::/64 | 2001:db8:1000:1521::15/64 | to e0/0 R21  |
|        |        | e0/3 | 2001:db8:1000:1520::/64 | 2001:db8:1000:1520::15/64 | to e0/0 R20  |   
|        |    R19 | e0/0 | 2001:db8:1000:1914::/64 | 2001:db8:1000:1914::19/64  | to e0/3 R14  |   
|        |    R20 | e0/0 | 2001:db8:1000:2015::/64 | 2001:db8:1000:2015::20/64| to e0/3 R21  |   
|        |    SW4 | e1/0 | 2001:db8:1000:412::/64 | 2001:db8:1000:412::4/64| to e0/0 R12  |   
|        |        | e1/1 | 2001:db8:1000:413::/64 | 2001:db8:1000:413::4/64| to e0/1 R13  |   
|        |    SW5 | e1/0 | 2001:db8:1000:513::/64 | 2001:db8:1000:513::5/64 | to e0/0 R13  |  
|        |        | e1/1 | 2001:db8:1000:512::/64 | 2001:db8:1000:512::5/64 | to e0/1 R12  |   

**Санкт-Петербург (ipv4 - 10.10.1.0/24)**

| Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|------------|-----------|----------|----------------|----------------|
|  R16   | e0/0 | 10.10.1.24/30 | 10.10.1.25/30 | to e0/3 SW10  |
|    | e0/1 | 10.10.1.4/30 | 10.10.1.6/30 | to e0/1 R18  | 
|    | e0/2 | 10.10.1.20/30  | 10.10.1.22/30  | to e1/0 SW9  |   
|    | e0/3 | 10.10.1.32/30 | 10.10.1.33/30 | to e0/0 R32 |   
| R17 | e0/0 | 10.10.1.36/30 | 10.10.1.37/30 | to e0/3 SW9 |   
|     | e0/1 | 10.10.1.16/30 | 10.10.1.17/30 | to e0/1 R18  |   
|     | e0/2 | 10.10.1.28/30 | 10.10.1.29/30 | to e1/0 SW10  |     
| R18 | e0/0 | 10.10.1.4/30  | 10.10.1.5/30  | to e0/1 R16  |   
|     | e0/1 | 10.10.1.16/30  | 10.20.2.18/30  | to e0/1 R17  |   
|     | e0/2 | 172.16.4.20/30 | 172.16.4.22/30  | to e0/3 R24  |
|     | e0/3 | 172.16.4.24/30  | 172.16.4.25/30  | to e0/3 R26  |   
| R32 | e0/0 | 10.10.1.32/30 | 10.10.1.34/30 | to e0/3 R16  |   


**Санкт-Петербург (ipv6 - 2001:db8:2000::/48)**

| Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|------------|-----------|----------|----------------|----------------|
|  R16  | e0/0 | 2001:db8:2000:1610::/64 | 2001:db8:2000:1610::16/64 | to e0/3 SW10  |
|    | e0/1 | 2001:db8:2000:1618::/64 | 2001:db8:2000:1618::16/64 | to e0/1 R18  | 
|    | e0/2 | 2001:db8:2000:169::/64  | 2001:db8:2000:169::16/64  | to e1/0 SW9  |   
|    | e0/3 | 2001:db8:2000:1632::/64 | 2001:db8:2000:1632::16/64  | to e0/0 R32 |   
| R17 | e0/0 | 2001:db8:2000:179::/64 | 2001:db8:2000:179::17/64 | to e0/3 SW9 |   
|     | e0/1 | 2001:db8:2000:1718::/64| 2001:db8:2000:1718::17/64 | to e0/1 R18  |   
|     | e0/2 | 2001:db8:2000:1710::/64 | 2001:db8:2000:1710::17/64 | to e1/0 SW10  |     
| R18 | e0/0 | 2001:db8:2000:1816::/64 | 2001:db8:2000:1816::18/64  | to e0/1 R16  |   
|     | e0/1 | 2001:db8:2000:1817::/64  | 2001:db8:2000:1817::18/64  | to e0/1 R17  |   
|     | e0/2 | 2001:db8:2000:1824::/64| 2001:db8:2000:1824::18/64  | to e0/3 R24  |
|     | e0/3 | 2001:db8:2000:1826::/64  | 2001:db8:2000:1826::18/64  | to e0/3 R26  |   
| R32 | e0/0 | 2001:db8:2000:3216::/64 | 2001:db8:2000:3216::32/64 | to e0/3 R16  |  


**Триада (ipv4 - 10.30.3.0/24)**

| Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|------------|-----------|----------|----------------|----------------|
|  R23   | e0/0 | 172.16.2.20/30 | 172.16.2.22/30 | to e0/2 R22  |
|    | e0/1 | 10.30.3.0/30 | 10.30.3.1/30 | to e0/0 R25  | 
|    | e0/2 | 10.30.3.8/30  | 10.30.3.10/30  | to e0/2 R24  |   
| R24 | e0/0 | 172.16.3.20/30 | 172.16.3.22/30 | to e0/2 R21 |   
|     | e0/1 | 10.30.3.20/30 | 10.30.3.22/30 | to e0/0 R26  |   
|     | e0/2 | 10.30.3.8/30 | 10.30.3.11/30 | to e0/2 R23  |     
|     | e0/3 | 172.16.4.20/30 | 172.16.4.21/30 | to e0/2 R18  | 
| R25 | e0/0 | 10.30.3.0/30  | 10.30.3.2/30  | to e0/1 R23  |   
|     | e0/1 | 172.16.1.24/30  | 172.16.1.25/30  | to e0/0 R27  |   
|     | e0/2 | 10.30.3.24/30 | 10.30.3.25/30  | to e0/2 R26  |
|     | e0/3 | 172.16.1.28/30  | 172.16.1.30/30  | to e0/1 R28  |   
| R26 | e0/0 | 10.30.3.32/30 | 10.30.3.21/30 | to e0/1 R24  |  
|     | e0/1 | 172.16.1.26/30  | 172.16.1.17/30  | to e0/0 R28 |   
|     | e0/2 | 10.30.3.24/30 | 10.30.3.26/30  | to e0/2 R25  |
|     | e0/3 | 172.16.4.24/30  | 172.16.4.26/30  | to e0/3 R18  | 

**Триада (ipv6 - 2001:db8:3000::/48)**

| Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|------------|-----------|----------|----------------|----------------|
|  R23   | e0/0 | 2001:db8:5000:2322::/64 | 2001:db8:5000:2322::23/64 | to e0/2 R22  |
|    | e0/1 | 2001:db8:3000:2325::/64 | 2001:db8:3000:2325::23/64 | to e0/0 R25  | 
|    | e0/2 | 2001:db8:3000:2324::/64  | 2001:db8:3000:2324::23/64  | to e0/2 R24  |   
| R24 | e0/0 | 2001:db8:7000:2421::/64 | 2001:db8:7000:2421::24/64 | to e0/2 R21 |   
|     | e0/1 | 2001:db8:3000:2426::/64 | 2001:db8:3000:2426::24/64 | to e0/0 R26  |   
|     | e0/2 | 2001:db8:3000:2423::/64 | 2001:db8:3000:2423::24/64 | to e0/2 R23  |     
|     | e0/3 | 2001:db8:2000:2418::/64 | 2001:db8:2000:2418::24/64 | to e0/2 R18  | 
| R25 | e0/0 | 2001:db8:3000:2523::/64  | 2001:db8:3000:2523::25/64 | to e0/1 R23  |   
|     | e0/1 | 2001:db8:8000:2527::/64  | 2001:db8:8000:2527::25/64  | to e0/0 R27  |   
|     | e0/2 | 2001:db8:3000:2526::/64 | 2001:db8:3000:2526::25/64  | to e0/2 R26  |
|     | e0/3 | 2001:db8:4000:2528::/64  | 2001:db8:4000:2528::25/64  | to e0/1 R28  |   
| R26 | e0/0 | 2001:db8:3000:2624::/64 | 2001:db8:3000:2624::26/64 | to e0/1 R24  |  
|     | e0/1 | 2001:db8:4000:2628::/64  | 2001:db8:4000:2628::26/64 | to e0/0 R28 |   
|     | e0/2 | 2001:db8:3000:2625::/64 | 2001:db8:3000:2625::26/64  | to e0/2 R25  |
|     | e0/3 | 2001:db8:2000:2618::/64  | 2001:db8:2000:2618::26/64   | to e0/3 R18  | 

**Киторн и Ламас**

| ipv4 | Имя уст-ва | Интерфейс | Адрес сети | IPv4 адрес | Пояснение |
|--------|------------|-----------|----------|----------------|----------------|
|        |  R21   | e0/0 | 172.16.5.20/30 | 172.16.5.21/30 | to e0/2 R15  |
|        |    | e0/1 | 172.16.6.20/30 | 172.16.6.21/30 | to e0/1 R22  | 
|        |        | e0/2 | 172.16.3.20/30 | 172.16.3.21/30  | to e0/0 R24  |     
|        | R22 | e0/0 | 172.16.2.20/30 | 172.16.7.22/30 | to e0/2 R14  |   
|        |   | e0/1 | 172.16.2.20/30 | 172.16.6.22/30 | to e0/1 R21   |   
|        |   | e0/2 | 172.16.2.20/30 | 172.16.2.22/30 | to e0/0 R23   |   
|  ipv6  |   |      |           | | |
|        |   R21 | e0/0 | 2001:db8:1000:2115::/64 | 2001:db8:1000:2115::21/64  | to e0/2 R15   |   
|        |         | e0/1 | 2001:db8:6000:2122::/64 | 2001:db8:6000:2122::21/64  | to e0/1 R22 |   
|        |         | e0/2 | 2001:db8:7000:2124::/64 | 2001:db8:7000:2124::21/64  | to e0/0 R24   | 
|        | R22 | e0/0 | 2001:db8:1000:2214::/64 | 2001:db8:1000:2214::22/64 | to e0/2 R14  |   
|        |     | e0/1 | 2001:db8:6000:2221::/64| 2001:db8:6000:2221::22/64 | to e0/1 R21  |  
|        |     | e0/2 | 2001:db8:5000:2223::/64 | 2001:db8:5000:2223::22/64 | to e0/0 R23  |  
------------------------------------------------------------------------------

### Настройка устройств

Для всех устройств аналогичными являются настройки конфигурации:

1. Имя устройства 
2. Пароли для входа
3. Баннер и время
4. Включение интерфейса и назначение адреса 

В отчете представлена настройка устройства R28. 

Задано имя устройству командой:

```
Router> enable
Router# config terminal
Router(config)#hostname R28
```

После отключения DNS были назначены пароли для входа в привилегированный режим, консоли и VTY.

```
R28(config)# no ip domain-lookup
R28(config)#enable secret router
R28(config)#line con 0
R28(config-line)#password eve
R28(config-line)#login
R28(config)#line vty 0 4
R28(config-line)#password eve
R28(config-line)#login
```
Пароли были зашифрованы в виде открытого текста и создан баннер, предупреждающий всех, кто обращается к устройству, о том, что несанкционированный доступ запрещен.

```
R28(config)# service password-encryption
R28(config)#banner m
R28(config)#banner motd `Authorized Users Only!`
```
Далее сохранена конфигурация и установлено время:

```
R1(config)# exit
R1# copy running-config startup-config
R1(config)#clock timezone CST +3
```

### Настройка VLAN

Также была произведена настройка VLAN. Для примера приведены настройки для филиала Чокурдах по таблице:

| Устройство |	VLAN |	Адресс |	Сеть | Пояснение |
|------------|-------|---------|---------|-----------|
|R28 |	10 |192.168.1.1 |	192.168.1.0/24	|Gateway vlan 10|
|R28 |	20 |192.168.2.2|	192.168.2.0/24	|Gateway vlan 20 |
|R28 | 33 | 10.3.1.9 |	10.3.1.0/29|	Gateway vlan 33 |
|S29 | 33 | 10.3.1.10 |	10.3.1.0/29	| manage SW29 |
|VPC30|	10 |	192.168.1.10	|192.168.1.0/24  |	Address VPC30|
|VPC31|	20 |	192.168.2.20	|192.168.2.0/24|	Address VPC31|


```
SW29#show vlan brief         

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et0/3, Et0/2
10   vpc30                            active    Et0/0
20   vpc31                            active    Et0/1
33   manage                           active    
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup 
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 
```

Для предотвращения broadcast штормов в L2 сегменте сети был настроен каждый VPC в каждом офисе в своем VLAN.

* В офисе Чокудрак для маршрутизации VLAN используется технология Router-on-a-stick  через сабинтерфейсы на роутере R28.

* В офисе Санкт-Петербург маршрутизация VLAN осуществляется сразу на коммутаторах SW9 и SW10 посредством виртуальных (SVI) интерфейсов который будут шлюзом по умолчанию для пользовательских хостов сети.

* В офисе Москва на L2 сегменте сети организована отказаустойчивость за счет избыточности линков с использованием технологии HSRP соответственно на L3 коммутаторах SW4 и SW5 помимо виртуальных интерфейсов VLAN подняты виртуальные шлюзы по умолчанию для соответсвующих VLAN.