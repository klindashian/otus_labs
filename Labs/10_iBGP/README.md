## Домашнее задание 10
### iBGP

### Цель
Настроить iBGP в офисе Москва
Настроить iBGP в сети провайдера Триада
Организовать полную IP связанность всех сетей


### Описание/Пошаговая инструкция выполнения домашнего задания:

- Настройка iBGP в офисом Москва между маршрутизаторами R14 и R15.
- Настройка iBGP в провайдере Триада, с использованием RR.
- Настройка офиса Москва так, чтобы приоритетным провайдером стал Ламас.
- Настройка офиса С.-Петербург так, чтобы трафик до любого офиса распределялся по двум линкам одновременно.
- Все сети в лабораторной работе должны иметь IP связность.


### Настройка iBGP в офисом Москва между маршрутизаторами R14 и R15

<img src="Labs/10_iBGP/lab10_101.png" alt="Схема" width="600"/>

Для настройки соседства по iBGP между R14 и R15 необходимо добавить каждому маршрутизатору адрес loopback (сетевая доступность обеспечена OSPF) соответствующего соседа в конфигурацию, а также указать что обмен осуществляется между loopback интерфейсами.

На роутерах R14, R15 настроены следующие LoopBack интерфейсы:

``` 
R14 - LoopBack 0 address 10.20.1.4
R15 - LoopBack 0 address 10.20.1.5
```
Протоколом внутреней маршрутизиции при помощи которого будет проходить информация по iBGP является ospf. Для этого интерфейс LoopBack на роутерах R14, R15 добавлен в анонс ospf

```
interface Loopback0
  ip ospf 1 area 0
```  
Установим соседство по iBGP через Loopback интерфейсы роутеров.

```
R14(config-router)#neighbor 10.20.1.5 remote-as 1001
R14(config-router)#neighbor 10.20.1.5 update-source L1
```
```
R15(config-router)#neighbor 10.20.1.4 remote-as 1001
R15(config-router)#neighbor 10.20.1.4 update-source L1
```

После введеных команд выполним проверку на R14:

``` 
R14(config-router)#do sh ip bgp sum
BGP router identifier 10.20.1.4, local AS number 1001
BGP table version is 6, main routing table version 6
3 network entries using 432 bytes of memory
4 path entries using 320 bytes of memory
4/3 BGP path/bestpath attribute entries using 608 bytes of memory
2 BGP AS-PATH entries using 64 bytes of memory
0 BGP route-map cache entries using 0 bytes of memory
0 BGP filter-list cache entries using 0 bytes of memory
BGP using 1424 total bytes of memory
BGP activity 3/0 prefixes, 4/0 paths, scan interval 60 secs

Neighbor        V           AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
10.20.1.5       4         1001       5       8        6    0    0 00:00:21        1 172.16.5.21     4          101      12       8        5    0    0 00:03:20        1

```
Между R14 и R15 установилось iBGP соседство.

### Настройка iBGP в провайдере Триада, с использованием RR 
Выполнение этой части заключается в настройке iBGP между маршрутизаторами R23, R24, R25, R26 с изпользованием Route-Reflector. Роль RR-server`а будет выполнять R24, а остальные RR-client. Для Триады в качестве Loopback адресов в каждом октете указан номер устройства.

Создадим на R24 peer-group где в качестве соседей укажем R23, R25, R26 (nакже настроим обмен информацией между loopback интерфейсами и next-hop для peer-group):

```  
R24#sh run | s bgp
router bgp 520
 bgp router-id 24.24.24.24
 bgp log-neighbor-changes
 neighbor RR-CLIENT peer-group
 neighbor RR-CLIENT remote-as 520
 neighbor RR-CLIENT update-source Loopback1
 neighbor RR-CLIENT route-reflector-client
 neighbor RR-CLIENT next-hop-self
 neighbor 23.23.23.23 peer-group RR-CLIENT
 neighbor 25.25.25.25 peer-group RR-CLIENT
 neighbor 26.26.26.26 peer-group RR-CLIENT
 neighbor 172.16.3.21 remote-as 301
 neighbor 172.16.4.22 remote-as 2042
```

На остальных маршрутизаторах необходимо поднять соседство с R24. Так как конфигурация на клиентах одинакова для примера берется R23:

```
R23(config)#do sh run | s bgp
router bgp 520
 bgp log-neighbor-changes
 neighbor 24.24.24.24 remote-as 520
 neighbor 24.24.24.24 update-source Loopback1
``` 
Для проверки будет просмотрен вывод команды `show ip bgp summary` на RR-server R24 и RR-client R25:

```
R24#sh ip bgp sum
BGP router identifier 24.24.24.24, local AS number 520
BGP table version is 2, main routing table version 2
1 network entries using 144 bytes of memory
1 path entries using 80 bytes of memory
1/1 BGP path/bestpath attribute entries using 152 bytes of memory
1 BGP AS-PATH entries using 24 bytes of memory
0 BGP route-map cache entries using 0 bytes of memory
0 BGP filter-list cache entries using 0 bytes of memory
BGP using 400 total bytes of memory
BGP activity 1/0 prefixes, 1/0 paths, scan interval 60 secs

Neighbor          V           AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
23.23.23.23       4          520       9      10        2    0    0 00:04:54        3
25.25.25.25       4          520       6       9        2    0    0 00:02:55        0
26.26.26.26       4          520       5       9        2    0    0 00:02:54        2
172.16.3.21       4          301     164     163        2    0    0 02:25:10        5
172.16.4.22       4         2042     163     163        2    0    0 02:25:05        2
```

```
R25#sh ip bgp summary
BGP router identifier 10.10.1.4, local AS number 520
BGP table version is 1, main routing table version 1
1 network entries using 144 bytes of memory
1 path entries using 80 bytes of memory
1/0 BGP path/bestpath attribute entries using 152 bytes of memory
1 BGP AS-PATH entries using 24 bytes of memory
0 BGP route-map cache entries using 0 bytes of memory
0 BGP filter-list cache entries using 0 bytes of memory
BGP using 400 total bytes of memory
BGP activity 1/0 prefixes, 1/0 paths, scan interval 60 secs

Neighbor          V           AS MsgRcvd MsgSent   TblVer  InQ OutQ Up/Down  State/PfxRcd
24.24.24.24       4          520       9       5        1    0    0 00:02:40        1
```
Как видно из результатов проверки RR-server(R24) установил сессии с тремя остальными RR-client(R23, R25, R26), а RR-client(R25) только с RR-server(R25).

### Настройка офиса Москва так, чтобы приоритетным провайдером стал Ламас.

Перед началом выполнения настройки необходимо проверить, как идет трафик с R14(AS 1001 Москва) к R18(AS 2042 СПб):

``` 
R14#sh ip bgp
BGP table version is 4, local router ID is 10.20.1.4
Status codes: s suppressed, d damped, h history, * valid, > best, i - internal,
              r RIB-failure, S Stale, m multipath, b backup-path, f RT-Filter,
              x best-external, a additional-path, c RIB-compressed,
Origin codes: i - IGP, e - EGP, ? - incomplete
RPKI validation codes: V valid, I invalid, N Not found

     Network            Next Hop            Metric LocPrf Weight Path
 *>  10.20.1.4/32        0.0.0.0                  0         32768 i
 *>  10.20.1.5/32       10.20.2.34                11         32768 i
 *>  10.20.2.0/28      172.16.5.21                             0 101 301 520 2042
``` 
``` 
R14#sh ip route | in B
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
B        172.16.1.16/30  [20/0]   via 172.16.5.21, 00:05:13
B        172.16.2.20/30  [20/0]   via 172.16.5.21, 01:08:55
B        172.16.3.20/30  [20/0]   via 172.16.5.21, 00:15:22
B        172.16.4.20/30  [20/0]   via 172.16.5.21, 01:17:15
B        172.16.4.24/30  [20/0]   via 172.16.5.21, 01:15:56
B        172.16.5.20/30  [200/0]   via 10.10.1.5, 00:05:13
B        172.16.6.20/30  [20/0]   via 172.16.5.21, 00:04:26
``` 
Как видно из вывода команд выбирается маршрут через AS 101 Киторн, необходимо сделать чтобы трафик шел через AS 301 Ламас.

Для того что бы приоритетным провайдером стала AS301 (Ламас) используем атрибут Local Preference, который применяется для управления исходящим трафиком BGP.

После необходимо применить этот route-map к R15 для того чтобы маршруты получаемые через этот маршрутизатор, а соответственно маршруты через AS 301 Ламас получали больший приоритет чем маршруты приходящие от AS 101 Киторн:

``` 
R15(config)# route-map AS301 permit 10
R15(config-route-map)# set local-preference 150
R15(config-router)# neighbor 172.16.5.21 route-map AS301 in
```
Также AS 1001 Москва необходимо настроить маршрутизаторы как next-hop по отношению друг другу:

```
router bgp 1001
 neighbor 10.10.1.4 next-hop-self

router bgp 1001
 neighbor 10.10.1.5 next-hop-self

```
Проведем проверку:

``` 
R14#sh ip route | in B
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
B        172.16.1.16/30  [20/0]   via 172.16.5.21, 00:01:16
B        172.16.2.20/30  [20/0]   via 172.16.5.21, 00:01:16
B        172.16.3.20/30  [20/0]   via 172.16.5.21, 00:01:16
B        172.16.4.20/30  [20/0]   via 172.16.5.21, 00:01:16
B        172.16.4.24/30  [20/0]   via 172.16.5.21, 00:01:16
B        172.16.5.20/30  [200/0]   via 10.10.1.5,  00:00:53
B        172.16.6.20/30  [20/0]   via 172.16.5.21, 00:01:16

```
Видно, что среди двух вариантов маршрутов во внешние сети из AS1001 Москва выбран маршрут через AS 301 Ламас.

### Настройте офис С.-Петербург так, чтобы трафик до любого офиса распределялся по двум линкам одновременно.

<img src="Labs/10_iBGP/lab10_2042.png" alt="Схема" width="600"/>

``` 
R18#sh ip route | in B
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
B        10.20.2.0/24 [20/0] via 172.16.4.21  00:21:54
B        172.16.1.16/30  [20/0]   via 172.16.4.21, 00:00:35
B        172.16.2.20/30  [20/0]   via 172.16.4.21, 00:00:35
B        172.16.3.24/30  [20/0]   via 172.16.4.21, 00:00:35
B        172.16.5.20/30  [20/0]   via 172.16.4.21, 00:00:35
B        172.16.6.20/30  [20/0]   via 172.16.4.21, 00:00:35
B        172.16.7.20/30  [20/0]   via 172.16.4.21, 00:00:35
```

Сейчас таблице маршрутизации есть только 1 маршрут до интерфейса R24(AS 520 Триада).

Для выполнения условия задания необходимо настроить на данном маршрутизаторе балансировку.
Включается балансировка следующим образом:

``` 
router bgp 2042
 bgp bestpath as-path multipath-relax
 maximum-paths 2
```
Далее выполним такую же проверку как и в начале:
``` 
R18#sh ip route bgp
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 6 subnets, 3 masks
B        10.20.2.0/24 [20/0] via 172.16.4.26, 00:09:07
                     [20/0] via 172.16.4.21, 00:09:07
      172.16.0.0/16 is variably subnetted, 10 subnets, 2 masks
B        172.16.1.16/30  [20/0]   via 172.16.4.26, 00:07:35
                                  via 172.16.4.21, 00:07:35  
B        172.16.2.20/30  [20/0]   via 172.16.4.26, 00:07:35
                                  via 172.16.4.21, 00:07:35
B        172.16.3.24/30  [20/0]   via 172.16.4.26, 00:07:35
                                  via 172.16.4.21, 00:07:35
B        172.16.5.20/30  [20/0]   via 172.16.4.26, 00:07:35
                                  via 172.16.4.21, 00:07:35
B        172.16.6.20/30  [20/0]   via 172.16.4.26, 00:07:35
                                  via 172.16.4.21, 00:07:35
B        172.16.7.20/30  [20/0]   via 172.16.4.26, 00:07:35
                                  via 172.16.4.21, 00:07:35
```

### Все сети в лабораторной работе должны иметь IP связность

Для выполнения задания была выполнена проверка доступности из Москвы до устройств R27, R28.

``` 
R14#ping 172.16.1.29
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.16.1.29, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 1/1/2 ms

R14#trace 172.16.1.29
Type escape sequence to abort.
Tracing the route to 172.16.1.29
VRF info:  (vrf in name/id, vrf out name/id) 
    1 10.20.2.34    2 msec 0 msec 1 msec
    2 172.16.5.21   0 msec 0 msec 1 msec
    3 172.16.3.22   [AS 301]  0 msec 1 msec 1 msec
    4 10.30.3.10    1 msec 0 msec 1 msec
    5 10.30.3.2     1 msec 0 msec 1 msec
    6 172.16.1.29   [AS 520]  2 msec *  1 msec
```

``` 
R15#ping 172.16.1.26
Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 172.16.1.26, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 1/1/1 ms

R15#trace 172.16.1.26
Type escape sequence to abort.
Tracing the route to 172.16.1.26
VRF info:  (vrf in name/id, vrf out name/id) 
    1 172.16.5.21   0 msec 1 msec 0 msec
    2 172.16.3.22   [AS 301]  0 msec 0 msec 1 msec
    3 10.30.3.22    1 msec 0 msec 1 msec
    4 10.30.3.25    1 msec 0 msec 1 msec
    5 172.16.1.26   [AS 520]  1 msec *  1 msec
```