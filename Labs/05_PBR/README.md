## Домашнее задание 5
### PBR

### Цель:
1. Настроить политику маршрутизации в офисе Чокурдах
2. Распределить трафик между 2 линками


### Описание/Пошаговая инструкция выполнения домашнего задания:

- Настройка политики маршрутизации для сетей офиса.
- Распределение трафика между двумя линками с провайдером.
- Настройка отслеживания линка через технологию IP SLA.(только для IPv4)
- Настройка для офиса Лабытнанги маршрута по-умолчанию.

Ниже представлена схема взаимодействия в офисе Чокурдах и Лабытнанги

![Схема взаимодействия](Labs/05_PBR/lab5.png "Схема взаимодействия")

### Настройка для офиса Лабытнанги маршрута по-умолчанию

На R27 был прописан маршрут по умолчанию c помощью команды:

```
ip route 0.0.0.0 0.0.0.0 172.16.1.25
```

### Настройка маршрутизации для сетей офиса

Маршрутизации была настроена таким образом, чтобы трафик по умолчанию проходил от устройства R28 в офис к  R25, R26, с помощью настройки маршрутов по умолчанию с разными метриками командой `ip route [Destination prefix] [Destination prefix mask] [address or interface] [metric]`.

Для примера приведена настройка устройства R25 (маршрут до et0/1 R28 и et0/2 R26 для связи между соответвенно):

```
R25(config)#ip route 0.0.0.0 0.0.0.0 172.16.1.29 20
R25(config)#ip route 0.0.0.0 0.0.0.0 10.30.3.26 21

```

Для R28 прописаны маршруты для внешних интерфейсов R25 и R26:

``` 
R28(config)#ip route 0.0.0.0 0.0.0.0 172.16.1.17 20
R28(config)#ip route 0.0.0.0 0.0.0.0 172.16.1.30 21

```
Настройка маршрутизации сетевых устройств офиса сводится к указанию шлюза по умолчанию на каждом из них. После настройки проведена проверка доступности VPC30 до et0/2 роутера.

```  
VPC30> ping 192.168.1.1 

84 bytes from 192.168.1.1 icmp_seq=1 ttl=254 time=0.486 ms
84 bytes from 192.168.1.1 icmp_seq=2 ttl=254 time=0.453 ms
84 bytes from 192.168.1.1 icmp_seq=3 ttl=254 time=0.440 ms
84 bytes from 192.168.1.1 icmp_seq=4 ttl=254 time=0.560 ms
84 bytes from 192.168.1.1 icmp_seq=5 ttl=254 time=0.433 ms

```

### Распределение трафика между двумя линками с провайдером

Проведены настройки так, чтобы трафик от VPC30 проходил через устройство R26, а трафик VPC31 к R25.

Для настройки на первом этапе реализованы списки доступа на маршрутизаторе R28. Так как в предыдущем задании каждый VPC находится в своем VLAN, ACL настроены в соответствии с ними.

Пример настройки:
``` 
R28(config)#ip access-list standart VLAN_10
R28(config-ext-nacl)#permit 192.168.1.0 0.0.0.255
```

Просмотр списков доступа:

``` 
R28(config)#do sh access-lists
Standard IP access list VLAN_10
    10 permit 192.168.1.0, wildcard bits 0.0.0.255
Standard IP access list VLAN_20
    10 permit 192.168.2.0, wildcard bits 0.0.0.255

```

Вторым шагом является настройка маршрутных карт, next-hop настроены в соответсвии с распределением трафика. Подробнее в конфигурационном файле.

```
R28#sh route-map
route-map PBR, permit, sequence 10
  Match clauses:
    ip address (access-lists): VLAN_10
  Set clauses:
    ip next-hop 172.16.1.17
  Policy routing matches: 0 packets, 0 bytes
route-map PBR, permit, sequence 20
  Match clauses:
    ip address (access-lists): VLAN_20
  Set clauses:
    ip next-hop 172.16.1.30
  Policy routing matches: 0 packets, 0 bytes
```
Далее созданные карты прописаны на соответсвующих саб-интерфейсах для VLAN 10 (VPC30) и VLAN20(VPC31).

``` 
interface Ethernet0/2
 no shutdown
 no ip address
!
interface Ethernet0/2.10
 no shutdown
 description gate vlan10
 encapsulation dot1Q 10
 ip address 192.168.1.1 255.255.255.0
 ip policy route-map PBR
!
interface Ethernet0/2.20
 no shutdown
 description gate vlan20
 encapsulation dot1Q 20
 ip address 192.168.2.2 255.255.255.0
 ip policy route-map PBR

```

### Настройка отслеживания линка через технологию IP SLA.

Настройка отслеживания:

```
ip sla 1
 icmp-echo 172.16.1.17 source-ip 172.16.1.18
ip sla schedule 1 life forever start-time now
```
Было создано правило, если интерфейс будет недоступен, track выключит маршрут из маршрутизации, на котором он прекреплен.Создаем обьект отслеживания и добавляем на маршрут по умолчанию:

```
track 1 ip sla 1 reachability
 delay down 10 up 10

ip route 0.0.0.0 0.0.0.0 172.16.1.17 20 track 1
ip route 0.0.0.0 0.0.0.0 172.16.1.30 21

```
Cтатистика SLA:

```
R28#sh ip sla statistics
IPSLAs Latest Operation Statistics

IPSLA operation id: 1
        Latest RTT: 1 milliseconds
Latest operation start time: 23:20:10 UTC Mon May 06 2024
Latest operation return code: OK
Number of successes: 9
Number of failures: 0
Operation time to live: Forever
```
При отключение линка со стороны R26:

```
R28#sh ip sla statistics
IPSLAs Latest Operation Statistics

IPSLA operation id: 1
        Latest RTT: NoConnection/Busy/Timeout
Latest operation start time: 23:23:10 UTC Mon May 6 2024
Latest operation return code: Timeout
Number of successes: 11
Number of failures: 1
Operation time to live: Forever
```
После отключения интерфейса, маршрут отключается и проподает из таблицы маршрутизации.

``` 
R28#
*May 6 23:23:20.189: %TRACK-6-STATE: 1 ip sla 1 reachability Up -> Down
```