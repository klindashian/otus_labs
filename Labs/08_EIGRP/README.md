## Домашнее задание 8
### EIGRP


### Цель:
1. Настроить EIGRP в С.-Петербург;
2. Использовать named EIGRP


### Описание/Пошаговая инструкция выполнения домашнего задания:
- В офисе С.-Петербург настроить EIGRP.
- R32 получает только маршрут по умолчанию.
- R16-17 анонсируют только суммарные префиксы.
- Использовать EIGRP named-mode для настройки сети.
- Настройка осуществляется одновременно для IPv4 и IPv6.


<img src="Labs/08_EIGRP/lab8.png" alt="Схема" width="600"/>

**В схеме были поменяны адреса для удобства, подробнее в конфиге**

### В офисе С.-Петербург настроить EIGRP.

В ходе настройки были включены на маршрутизаторах named-mode EIGRP:

``` 
R18(config)#router eigrp SPB
```

Далее заданы сети IPv4 для EIGRP (так как все интерфейсы в локации СПБ находятся в сети 10.10.1.0/24) настройки выглядят следующим образом:

``` 
R18(config-router-af)#network 10.10.1.0 0.0.0.255
```

Такжe задан router-id для каждого маршрутизатора для IPv4 и IPv6:

```
R18(config-router-af)#eigrp router-id 18.18.18.18
```
В данной работе в качестве Router-ID в каждом  октете указан номер устройства.
Аналогично настраивается на остальных маршрутизаторах.

### R32 получает только маршрут по умолчанию.
Для выполнения настройки необходимо объявить маршрут по умолчанию для IPv4 и IPv6 на пограничном маршрутизаторе R18.

``` 
R18(config)#ip route 0.0.0.0 0.0.0.0 172.16.4.21 
R18(config)#ipv6 route ::/0 2001:db8:2000:2418::24
```
Экземпляр будет создан при настройке адресного семейства и номера автономной системы

``` 
R18(config-router)#address-family ipv4 unicast autonomous-system 1
R18(config-router)#address-family ipv6 unicast autonomous-system 1 
```

Команда `redistribute static`` указывает протоколу EIGRP включить статические маршруты в обновления EIGRP для других маршрутизаторов.

``` 
address-family ipv4 unicast autonomous-system 1
topology base
redistribute static
```

После всех настроек таблица маршрутизации на R32:

``` 
R32#sh ip route eigrp
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is 10.10.1.29 to network 0.0.0.0

D*EX  0.0.0.0/0 [170/2048000] via 10.10.1.29, 00:00:49, Ethernet0/0
      10.0.0.0/8 is variably subnetted, 8 subnets, 4 masks
D        10.10.1.0/30 [90/2560000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        10.10.1.4/30 [90/2560000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        10.10.1.8/30 [90/2048000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        10.10.1.12/30 [90/1536000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        10.10.1.16/30 [90/1536000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        10.10.1.20/30 [90/2560000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        10.10.1.24/30 [90/2560000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        192.168.3.0/24 [90/2560000] via 10.10.1.29, 00:20:16, Ethernet0/0
D        192.168.4.0/24 [90/2560000] via 10.10.1.29, 00:20:16, Ethernet0/0

R32#sh ipv6 route eigrp
IPv6 Routing Table - default - 10 entries
Codes: C - Connected, L - Local, S - Static, U - Per-user Static route
       B - BGP, HA - Home Agent, MR - Mobile Router, R - RIP
       H - NHRP, I1 - ISIS L1, I2 - ISIS L2, IA - ISIS interarea
       IS - ISIS summary, D - EIGRP, EX - EIGRP external, NM - NEMO
       ND - ND Default, NDp - ND Prefix, DCE - Destination, NDr - Redirect
       O - OSPF Intra, OI - OSPF Inter, OE1 - OSPF ext 1, OE2 - OSPF ext 2
       ON1 - OSPF NSSA ext 1, ON2 - OSPF NSSA ext 2, la - LISP alt
       lr - LISP site-registrations, ld - LISP dyn-eid, a - Application
EX  ::/0 [170/2048000]
     via FE80::16, Ethernet0/0
D   2001:DB8:2000:179::/64 [90/2048000]
     via FE80::16, Ethernet0/0
D   2001:DB8:2000:1710::/64 [90/1536000]
     via FE80::16, Ethernet0/0
D   2001:DB8:2000:1716::/64 [90/2048000]
     via FE80::16, Ethernet0/0
D   2001:DB8:2000:1718::/64 [90/2048000]
     via FE80::16, Ethernet0/0

```
Как видно в таблице маршрутизации содержатся все маршруты полученные посредством EIGRP, для того чтобы ограничить R32 так, чтобы он получал только маршрут по умолчанию необходимо создать ACL списки доступа для ipv4 и prefix-list для ipv6:

```
R32#sh ipv6 prefix-list IN
ipv6 prefix-list IN: 1 entries
seq 5 permit ::/0

R32#sh access-list 2
Standard IP access list 2
10 permit 0.0.0.0 (1 match)
20 deny   any (10 matches)
```

Далее настройки применены к интерфейсу. Подробнее в конфигурационном файле.

``` 
router eigrp SPB
 !
 address-family ipv4 unicast autonomous-system 1
  !
  topology base
   distribute-list 2 in Ethernet0/0
  exit-af-topology
  network 10.10.1.0 0.0.0.255
  eigrp router-id 32.32.32.32
 exit-address-family

``` 

После этого проверяем маршруты:

```  
R32#sh ip route eigrp
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is 10.10.1.29 to network 0.0.0.0

D*EX  0.0.0.0/0 [170/2048000] via 10.10.1.29, 01:29:58, Ethernet0/0

R32#sh ipv6 route eigrp
IPv6 Routing Table - default - 6 entries
Codes: C - Connected, L - Local, S - Static, U - Per-user Static route
       B - BGP, HA - Home Agent, MR - Mobile Router, R - RIP
       H - NHRP, I1 - ISIS L1, I2 - ISIS L2, IA - ISIS interarea
       IS - ISIS summary, D - EIGRP, EX - EIGRP external, NM - NEMO
       ND - ND Default, NDp - ND Prefix, DCE - Destination, NDr - Redirect
       O - OSPF Intra, OI - OSPF Inter, OE1 - OSPF ext 1, OE2 - OSPF ext 2
       ON1 - OSPF NSSA ext 1, ON2 - OSPF NSSA ext 2, la - LISP alt
       lr - LISP site-registrations, ld - LISP dyn-eid, a - Application
EX  ::/0 [170/2048000]
     via FE80::16, Ethernet0/0
``` 

По выводу команды видно, что  в таблице остались только маршруты по умолчанию.

### R16-17 анонсируют только суммарные префиксы.

Проверка до настроек:

``` 
R18#sh ip route eigrp
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 8 subnets, 4 masks
D        10.10.1.0/30  [90/1541120] via 10.10.1.21, 00:10:24, Ethernet0/1
D        10.10.1.4/30  [90/1541120] via 10.10.1.25, 00:10:24, Ethernet0/0
D        10.10.1.8/30  [90/2048000] via 10.10.1.25, 00:10:24, Ethernet0/0
                        [90/2048000] via 10.10.1.21, 00:10:24, Ethernet0/1
D        10.10.1.12/30  [90/1536000] via 10.10.1.21, 00:20:51, Ethernet0/1
D        10.10.1.16/30  [90/1536000] via 10.10.1.25, 00:10:24, Ethernet0/0
D        10.10.1.28/30  [90/1536000] via 10.10.1.25, 00:10:24, Ethernet0/0
D        192.168.3.0/24 [90/1541120] via 10.10.1.25, 00:03:16, Ethernet0/0
                        [90/1541120] via 10.10.1.21, 00:03:16, Ethernet0/1
D        192.168.4.0/24 [90/1541120] via 10.10.1.25, 00:03:16, Ethernet0/0
                        [90/1541120] via 10.10.1.21, 00:03:16, Ethernet0/1

```

Чтобы включить автоматическое объединение для EIGRP, необходимо использовать команду `auto-summary` в режиме конфигурации маршрутизатора, однако этот метод не заработал. Добавила вручную как показано в командах ниже:

``` 
R17(config)#router eigrp SPB
R17(config-router)#address-family ipv4 unicast autonomous-system 1
R17(config-router-af)#af-interface ethernet 0/1
R17(config-router-af-interface)#summary-address 10.10.1.0 255.255.255.240

R16(config)# router eigrp SPB
R17(config-router)#address-family ipv4 unicast autonomous-system 1
R17(config-router-af)#af-interface ethernet 0/1
R17(config-router-af-interface)#summary-address 10.10.1.0 255.255.255.240
```
Для суммирования маршрутов использовалась 28 маска (255.255.255.240), для суммирования подсетей 10.10.1.0/30 - .12/30 в общую сеть, так как если использовать 27 маску, то под ее значение попадут connected сети роутера R18.

Проверка после суммирования:

``` 
R18#sh ip route eigrp

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 8 subnets, 4 masks
D        10.10.1.0/28 [90/1536000] via 10.10.1.25, 00:00:16, Ethernet0/0
                      [90/1536000] via 10.10.1.21, 00:00:16, Ethernet0/1
D        10.10.1.16/30  [90/1536000] via 10.10.1.25, 01:00:15, Ethernet0/0
D        10.10.1.28/30  [90/1536000] via 10.10.1.25, 01:00:15, Ethernet0/0
D        192.168.3.0/24 [90/1541120] via 10.10.1.25, 00:01:52, Ethernet0/0
                        [90/1541120] via 10.10.1.21, 00:01:52, Ethernet0/1
D        192.168.4.0/24 [90/1541120] via 10.10.1.25, 00:01:52, Ethernet0/0
                        [90/1541120] via 10.10.1.21, 00:01:52, Ethernet0/1

```

Видно, что таблица маршрутизации сократилась в соответствии с настройкой.