## Домашняя работа №2
### Развертывание коммутируемой сети с резервными каналами

### Задание
1. Создание сети и настройка основных параметров устройства
2. Выбор корневого моста
3. Наблюдение за процессом выбора протоколом STP порта, исходя из стоимости портов
4. Наблюдение за процессом выбора протоколом STP порта, исходя из приоритета портов


##### Таблица сетевых адресов

| Устройство | Интерфейс | IP адрес | Маска подсети|
| --------|-----------|-------------|--------------|
| S1	| VLAN 1 |	192.168.1.1 | 255.255.255.0 |
| S2    | VLAN 1 | 192.168.1.2 | 255.255.255.0 |
| S3    | VLAN 1 |192.168.1.3 |  255.255.255.0 |


Схема взаимодействия устройств представлена ниже

![Схема взаимодействия](Labs/02_STP/scheme.jpg)
### Решение

### 1 Создание сети и настройка основных параметров устройства

Для удобства приведены настройки только для устройства S1, так как для остальных устройств они аналогичны.

Задано имя устройству:

```
switch> enable
switch# config terminal
switch(config)# hostname S1
```
После отключения DNS были назначены пароли для входа в привилегированный режим, консоли и VTY. 
```
S1(config)# no ip domain-lookup
```
```
S1(config)# enable secret class
```
```
S1(config)# line console 0
S1(config-line)# password cisco
S1(config-line)# login

S1(config)# line vty 0 4
S1(config-line)# password cisco
S1(config-line)# login

```
Произведена настройка logging synchronous для консольного канала:
```
S1(config)#line console 0
S1(config-line)#logging synchronous  
```
Далее зашифрованы пароли и прописаны баннеры:

```
S1(config)# service password-encryption
S1(config)# banner motd `Authorized Users Only!`
```
Следующим шагом задан IP-адрес, в соответсвии с таблицей адресации для VLAN 1 на коммутаторе:
```
S1(config)#int vlan 1
S1(config-if)#ip address 192.168.1.1 255.255.255.0
S1(config-if)#no shutdown 
```
Последним шагом было копирование текущей конфигурации в файл загрузочной конфигурации:

```
S1#copy running-config startup-config
Destination filename [startup-config]? 
Building configuration...
Compressed configuration from 790 bytes to 546  bytes[OK]
```
Далее была проведена проверка способности компьютеров обмениваться эхо-запросами.

1. Успешно выполняется эхо-запрос от коммутатора S1 на коммутатор S2.

```
S2#ping 192.168.1.1Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 192.168.1.1, timeout is 2 seconds:.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 1/1/1 ms
```
2. Успешно выполняется эхо-запрос от коммутатора S1 на коммутатор S3.

```
S3#ping 192.168.1.1Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 192.168.1.1, timeout is 2 seconds:.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 1/1/1 msS3#
```
3. Успешно выполняется эхо-запрос от коммутатора S2 на коммутатор S3.

```
S2#ping 192.168.1.3Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 192.168.1.3, timeout is 2 seconds:.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 1/1/1 msS2#
```

### 2 Выбор корневого моста

На первом этапе были откючены все порты на коммутаторах.

```
S1(config)#int range ethernet 0/0-3
S1(config-if-range)#shu
S1(config-if-range)#shutdown 
```
Далее настроены подключенные порты в качестве транковых. На коммутаторе S1:
```
S1(config)#int range ethernet 0/0-3 
S1(config-if-range)#switchport trunk encapsulation dot1q 
S1(config-if-range)#switchport mode trunk 
```
И включены интерфейсы eth0/1 и eth0/3:

```
S1(config)#int et 0/1
S1(config-if)#no shutdown 
S1(config)#int et 0/3
S1(config-if)#no shu
S1(config-if)#no shutdown 
```
Аналогично для остальных устройств. Далее просмотрим данные протокола STP с помощью команды `show spanning-tree ` для устройств.


```
S1#show spanning-tree 

VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.1000
             This bridge is the root
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.1000
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/1               Desg FWD 100       128.2    Shr 
Et0/3               Desg FWD 100       128.4    Shr 

```

``` 
S2#show spanning-tree 

VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.1000
             Cost        100
             Port        4 (Ethernet0/3)
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.3000
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/1               Altn BLK 100       128.2    Shr 
Et0/3               Root FWD 100       128.4    Shr 
```

```
S3#show spanning-tree 

VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.1000
             Cost        100
             Port        2 (Ethernet0/1)
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.2000
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/1               Root FWD 100       128.2    Shr 
Et0/3               Desg FWD 100       128.4    Shr 

```

1. Какой коммутатор является корневым мостом? Почему этот коммутатор был выбран протоколом spanning-tree в качестве корневого моста? Коммутатор S1 является корневым. Корневой мост был выбран потому, что он имел наименьший идентификатор моста (значение приоритета + расширенный идентификатор системы (VLAN) + MAC-адрес коммутатора), именно MAC-адрес у него наименьший.

2. Какие порты на коммутаторе являются корневыми портами? Порт Et0/1 у S2 и порт Et0/3 у S3.

3. Какие порты на коммутаторе являются назначенными портами? Порт Et0/1 и Et0/3 у S1 и порт Et0/3 у S3.

4. Какой порт отображается в качестве альтернативного и в настоящее время заблокирован? Порт Et0/1 у S2. `Et0/1 Altn BLK `

5. Почему протокол spanning-tree выбрал этот порт в качестве невыделенного (заблокированного) порта? Алгоритм протокола spanning-tree (STA) использует корневой мост как точку привязки, после чего определяет, какие порты будут заблокированы, исходя из стоимости пути. Если затраты на путь равны, он сравнивает BID.Минимальный BID соседа S3:`32769 + aabb.cc00.2000`

### 3 Наблюдение за процессом выбора протоколом STP порта, исходя из стоимости портов

Помимо заблокированного порта, единственным активным портом на коммутаторе S2 является порт, назначенный корневым. Уменьшим стоимость этого корневого порта до 18, введя команду режима конфигурации интерфейса `Spanning-Tree Cost`

```
S2(config)#int et0/1
S2(config-if)#spanning-tree cost 18
```
Повторим вывод команды:

```
S3#show spanning-tree 

VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.1000
             Cost        100
             Port        1 (Ethernet0/0)
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.2000
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/0               Root FWD 100       128.1    Shr 
Et0/1               Altn BLK 100       128.2    Shr 
```
> Видно, что STP заблокировало порт, который был назначенным портом на коммутаторе S3

### 4 Наблюдение за процессом выбора протоколом STP порта, исходя из приоритета портов

После включения портов F0/1 и F0/3 на всех коммутаторах снова вызвана команда `show spanning-tree`
```
S2#show spanning-tree 

VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.1000
             Cost        100
             Port        3 (Ethernet0/2)
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.3000
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/0               Root FWD 100       128.1    Shr 
Et0/1               Altn BLK 100       128.2    Shr 
Et0/2               Altn BLK 100       128.3    Shr 
Et0/3               Altn BLK 100       128.4    Shr 
```

```
S3#show spanning-tree 

VLAN0001
  Spanning tree enabled protocol rstp
  Root ID    Priority    32769
             Address     aabb.cc00.1000
             Cost        100
             Port        1 (Ethernet0/0)
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec

  Bridge ID  Priority    32769  (priority 32768 sys-id-ext 1)
             Address     aabb.cc00.2000
             Hello Time   2 sec  Max Age 20 sec  Forward Delay 15 sec
             Aging Time  300 sec

Interface           Role Sts Cost      Prio.Nbr Type
------------------- ---- --- --------- -------- --------------------------------
Et0/0               Root FWD 100       128.1    Shr 
Et0/1               Altn BLK 100       128.2    Shr 
Et0/2               Desg FWD 100       128.3    Shr 
Et0/3               Desg FWD 100       128.4    Shr 
```

**Какой порт выбран протоколом STP в качестве порта корневого моста на каждом коммутаторе некорневого моста?** 

Et0/0 на коммутаторах S2 и S3

**Почему протокол STP выбрал эти порты в качестве портов корневого моста на этих коммутаторах?** 

Значение порта по умолчанию — 128; поэтому STP выбрал порт с меньшим номером в качестве корневого порта.


1.	Какое значение протокол STP использует первым после выбора корневого моста, чтобы определить выбор порта? Cost - cтоимость пути. Он выбирает путь с наименьшей накопленной стоимостью.
_______________________________________________________________________________________
2.	Если первое значение на двух портах одинаково, какое следующее значение будет использовать протокол STP при выборе порта? BID, выбрав меньшее значение.
_______________________________________________________________________________________
3.	Если оба значения на двух портах равны, каким будет следующее значение, которое использует протокол STP при выборе порта? Приоритет порта и номера порта соседа, более низкое значение является предпочтительным.
_______________________________________________________________________________________
