## Домашняя работа №3
### Внедрение DHCP версии IPv6

### Задание

1. Создание сети и настройка основных параметров устройства
2. Настройка и проверка двух серверов DHCPv6 на маршрутизаторе R1.
3. Настройка и проверка DHCP на маршрутизаторе R2.

Схема взаимодествия представлена в [отчете](https://gitlab.com/klindashian/otus_labs/-/blob/main/Labs/03_DHCP/README.md)

##### Таблица сетевых адресов

| Устройство | Интерфейс | IPv6 адрес | 
| --------|-----------|-------------|
| R1	| e0/0 | 2001:db8:acad:2::1 /64 |
|    | | fe80::1 |
|| e0/3	 |  2001:db8:acad:1::1 /64 |
|    | | fe80::1 |
| R2	| e0/0 | 2001:db8:acad:2::2 /64 |
|    | | fe80::1 |
|| e0/3	 |  2001:db8:acad:3::1 /64 |
|    | | fe80::1 |
|PC-A| 	NIC |	DHCP |	
|PC-B|	NIC |	DHCP |	

#### 1. Создание сети и настройка основных параметров устройств

Настройка параметров производилась аналогично для всех устройств, для удобства приведена настройка для устройства R1:

```
R1(config)# no ip domain-lookup
R1(config)#enable secret router
R1(config)#line con 0
R1(config-line)#password eve
R1(config-line)#login
R1(config)#line vty 0 4
R1(config-line)#password eve
R1(config-line)#login
```
Включим маршрутизацию IPv6

``` 
R1(config)#ipv6 unicast-routing
```
Далее произведена настройка IPv6 адресов в соответствии с таблицей:

``` 
R1(config-if)#int et0/0 
R1(config-if)#ipv6 address fe80::1 link-local 
R1(config-if)#ipv6 address 2001:db8:acad:2::1/64
R1(config-if)#no shutdown 
R1(config-if)#int et0/1                         
R1(config-if)#ipv6 address fe80::1 link-local
R1(config-if)#ipv6 address 2001:db8:acad:1::1/64
R1(config-if)#no shutdown
```
Проверим назначение адреса SLAAC с помощью R1.
Хост-компьютер получает IPv6-адрес, используя метод SLAAC. 

До настройки роутера:
``` 
VPCS> ip auto
No router answered ICMPv6 Router Solicitation
```
Получение адреса:
``` 
VPCS> ip auto  
GLOBAL SCOPE      : 2001:db8:acad:1:2050:79ff:fe66:6808/64
ROUTER LINK-LAYER : aa:bb:cc:00:b0:10
```
Подробнее:

```
VPCS> show ipv6

NAME              : VPCS[1]
LINK-LOCAL SCOPE  : fe80::250:79ff:fe66:6808/64
GLOBAL SCOPE      : 2001:db8:acad:1:2050:79ff:fe66:6808/64
DNS               : 
ROUTER LINK-LAYER : aa:bb:cc:00:b0:10
MAC               : 00:50:79:66:68:08
LPORT             : 20000
RHOST:PORT        : 127.0.0.1:30000
MTU:              : 1500
```

```
VPCS> show ipv6 all

NAME   IP/MASK                                 ROUTER LINK-LAYER  MTU
VPCS1  fe80::250:79ff:fe66:6808/64
       2001:db8:acad:1:2050:79ff:fe66:6808/64  aa:bb:cc:00:b0:10  1500
```
Часть полученного адреса, содержащая идентификатор хоста сгенерировалась на основе МАС адреса.

2. Настройка и проверка двух серверов DHCPv6 на маршрутизаторе R1.

Создим пул DHCP IPv6 на R1 с именем R1-STATELESS. Как часть этого пула, был назначен адрес DNS-сервера как 2001:db8:acad::1, а доменное имя как stateless.com.
``` 
R1(config-if)#ipv6 dhcp pool R1_Stateless
R1(config-dhcpv6)#dns-server 2001:db8:acad::254
R1(config-dhcpv6)#domain-name STATELESS.com
```
Далее настроим интерфейс eth0/1 на R1, чтобы предоставить другой флаг конфигурации для локальной сети R1, и укажем пул DHCP, который только что был создан, в качестве ресурса DHCP для этого интерфейса.
```
R1(config)#interface et0/1
R1(config-if)#ipv6 nd other-config-flag 
R1(config-if)#ipv6 dhcp server R1_Stateless
```
После перезагрузки устройства РС1 DNS не отобразился:
```
VPCS> show ipv6

NAME              : VPCS[1]
LINK-LOCAL SCOPE  : fe80::250:79ff:fe66:6808/64
GLOBAL SCOPE      : 2001:db8:acad:1:2050:79ff:fe66:6808/64
DNS               : 
ROUTER LINK-LAYER : aa:bb:cc:00:b0:10
MAC               : 00:50:79:66:68:08
LPORT             : 20000
RHOST:PORT        : 127.0.0.1:30000
MTU:              : 1500
```

Проверим доступность подключения интерфейса Et0/3 R2.

```
VPCS> ping  2001:db8:acad:3::1

2001:db8:acad:3::1 icmp6_seq=1 ttl=63 time=4.768 ms
2001:db8:acad:3::1 icmp6_seq=2 ttl=63 time=0.956 ms
2001:db8:acad:3::1 icmp6_seq=3 ttl=63 time=0.690 ms
2001:db8:acad:3::1 icmp6_seq=4 ttl=63 time=0.910 ms
2001:db8:acad:3::1 icmp6_seq=5 ttl=63 time=0.549 ms
```

Настройка DHCPv6-сервера с отслеживанием состояния на R1:

Создадим пул DHCPv6 на R1 для сети 2001:db8:acad:3:aaaa::/80

``` 
R1(config)#ipv6 dhcp pool R2_STATEFUL
R1(config-dhcpv6)#address prefix 2001:db8:acad:3:aaa::/80
R1(config-dhcpv6)#dns-server 2001:db8:acad::254
R1(config-dhcpv6)#domain-name stateful.com
R1(config)#interface et0/0
R1(config-if)#ipv6 dhcp server R2_STATEFUL
```
Проверка: 

``` 
VPCS> show ipv6

NAME              : VPCS[1]
LINK-LOCAL SCOPE  : fe80::250:79ff:fe66:6807/64
GLOBAL SCOPE      : 2001:db8:acad:3:2050:79ff:fe66:6807/64
DNS               : 
ROUTER LINK-LAYER : aa:bb:cc:00:40:30
MAC               : 00:50:79:66:68:07
LPORT             : 20000
RHOST:PORT        : 127.0.0.1:30000
MTU:              : 1500
``` 
В выходных данных используется префикс 2001:db8:acad:3::

#### 3 Настройте R2 в качестве агента ретрансляции DHCP 
Сконфигурируем команду ретрансляции dhcp ipv6 на интерфейсе R2.Также настроим команду managed-config-flag. 

``` 
R2(config)#int et0/3
R2(config-if)#ipv6 nd managed-config-flag 
R2(config-if)#ipv6 dhcp relay destination 2001:db8:acad:2::1 et0/0
```
Проверка назначения адреса после перезагрузки:

```
VPCS> show ipv6

NAME              : VPCS[1]
LINK-LOCAL SCOPE  : fe80::250:79ff:fe66:6806/64
GLOBAL SCOPE      : 2001:db8:acad:3:2050:79ff:fe66:6806/64
DNS               : 
ROUTER LINK-LAYER : aa:bb:cc:00:40:30
MAC               : 00:50:79:66:68:06
LPORT             : 20000
RHOST:PORT        : 127.0.0.1:30000
MTU:              : 1500
```
Проверим подключение, проверив IP-адрес интерфейса Et0/1 R1.

```
VPCS> ping 2001:db8:acad:1::1

2001:db8:acad:1::1 icmp6_seq=1 ttl=63 time=7.409 ms
2001:db8:acad:1::1 icmp6_seq=2 ttl=63 time=0.870 ms
2001:db8:acad:1::1 icmp6_seq=3 ttl=63 time=0.787 ms
2001:db8:acad:1::1 icmp6_seq=4 ttl=63 time=0.986 ms
2001:db8:acad:1::1 icmp6_seq=5 ttl=63 time=0.567 ms
```