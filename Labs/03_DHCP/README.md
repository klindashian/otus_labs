## Домашняя работа №3
### Внедрение DHCP

### Задание

1. Создание сети и настройка основных параметров устройства
2. Настройка и проверка двух серверов DHCPv4 на маршрутизаторе R1.
3. Настройка и проверка DHCP на маршрутизаторе R2.

##### Таблица сетевых адресов

| Устройство | Интерфейс | IP адрес | Маска подсети| Шлюз по умолчанию |
| --------|-----------|-------------|--------------|-------------|
| R1	| e0/0 |	10.0.0.1 | 255.255.255.252 |N/A|
|    | e0/1 | N/A | N/A |N/A |
|    | e0/1.100 |192.168.1.1 |  255.255.255.192 |N/A |
|    | e0/1.200 |192.168.1.65 |  255.255.255.224 |N/A |
|    | e0/1000 | N/A | N/A |N/A |
|R2	| e0/0	 | 10.0.0.2 |	255.255.255.252	| N/A |
||e0/3	 |192.168.1.97	|255.255.255.240	|N/A |
| S1 |	VLAN200 |	192.168.1.66	| 255.255.255.224	| 192.168.1.95|
| S2 | 	VLAN1	 | 192.168.1.98	| 255.255.255.240	| 192.168.1.97 |
|PC-A| 	NIC |	DHCP |	DHCP	| DHCP |
|PC-B|	NIC |	DHCP |	DHCP	| DHCP |
Схема взаимодействия устройств представлена ниже

![Схема взаимодействия](Labs/03_DHCP/scheme.jpeg)

### Решение

### 1 Создание сети и настройка основных параметров устройства

- “Подсеть A”, поддерживающая 58 хостов (клиентская VLAN на R1): 192.168.1.0/26 подсеть подддерживает 64 хостов самая маленькая подсеть, которая вмещает 58 хостов. Первый IP-адрес в таблице адресации для R1 G0/0/1.100: 192.168.1.1

- “Подсеть B”, поддерживающая 28 хостов (управляющая VLAN на R1). 

- “Подсеть C”, поддерживающая 12 хостов (клиентская сеть на R2).

Для удобства приведены настройки только для устройства S1, так как для остальных устройств они аналогичны.

Задано имя устройству:

```
Router> enable
Router# config terminal
Router(config)# hostname R1
```
После отключения DNS были назначены пароли для входа в привилегированный режим, консоли и VTY. 
```
R1(config)# no ip domain-lookup
```
```
R1(config)# enable secret class
```
```
R1(config)# line console 0
R1(config-line)# password cisco
R1(config-line)# login

S1(config)# line vty 0 4
S1(config-line)# password cisco
S1(config-line)# login
```
Далее зашифрованы пароли и прописаны баннеры:
```
R1(config)#service password-encryption 
R1(config)#banner motd `Authorized Users Only!`
```
Далее сохранена текущая конфигурация в файл конфигурации.

### 2 Настройте маршрутизацию между VLAN на R1

Сначала необходимо активировать интерфейс Et0/1 на маршрутизаторе.
```
R1(config)#int et0/1      
R1(config-if)#no shutdown 
```

Далее настроим подинтерфейсы для каждой VLAN в соответствии с требованиями таблицы IP-адресации. Все подинтерфейсы используют инкапсуляцию 802.1Q, и им присваивается первый доступный адрес из пула IP-адресов. 
``` 
R1(config-if)#int et0/1.100
R1(config-subif)#description Default Geteway for VLAN 100
R1(config-subif)#encapsulation dot1Q 100
R1(config-subif)#ip address 192.168.1.1 255.255.255.192
R1(config-subif)#exit
R1(config)#interface et0/1.200
R1(config-subif)#description Default Geteway for VLAN 200
R1(config-subif)#encapsulation dot1Q 200
R1(config-subif)#ip address 192.168.1.65 255.255.255.224
R1(config-subif)#exit
R1(config)#int et0/1.1000
R1(config-subif)#description Default Geteway for VLAN 1000
R1(config-subif)#encapsulation dot1Q 1000 native
R1(config-subif)#exit

```

Убедимся, что подинтерфейсы работают с помощью `show ip interface brief`
``` 
R1#show ip interface brief 
Interface                  IP-Address      OK? Method Status                Protocol
Ethernet0/0                unassigned      YES unset  administratively down down    
Ethernet0/1                unassigned      YES unset  up                    up      
Ethernet0/1.100            192.168.1.1     YES manual up                    up      
Ethernet0/1.200            192.168.1.65    YES manual up                    up      
Ethernet0/1.1000           unassigned      YES unset  up                    up      
Ethernet0/2                unassigned      YES unset  administratively down down    
Ethernet0/3                unassigned      YES unset  administratively down down 

```

Далее аналогично были настроены интерфейсы Et0/3 на R2, затем Et0/0 и статическая маршрутизацию для обоих маршрутизаторов

``` 
R2(config)#int et0/3
R2(config-if)#ip address 192.168.1.97 255.255.255.240
R2(config-if)#no shutdown
Настройте интерфейс Et0/0 для каждого маршрутизатора на основе приведенной выше таблицы IP-адресации. R2:
R2(config)#int et0/0
R2(config-if)#ip address 10.0.0.2 255.255.255.252
R2(config-if)#no shutdown
```
Аналогично:
``` 
R1(config)#int et0/0
R1(config-if)#ip address 10.0.0.1 255.255.255.252
R1(config-if)#no shutdown
```

Настройка маршрутов по умолчанию на каждом маршрутизаторе, указанном на IP-адрес Et0/0 на другом маршрутизаторе:
```
R1(config)#ip route 0.0.0.0 0.0.0.0 10.0.0.2
R2(config)#ip route 0.0.0.0 0.0.0.0 10.0.0.1 
```

Настройки основных параметров для каждого коммутатора произведены аналогичным образом.


#### Создание VLAN на S1.
Примечание: S2 настроен только с базовыми настройками.

Далее настроим VLAN на коммутаторе в соответсвии с таблицей.

```
S1#show vlan brief 

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et0/0, Et0/1, Et0/2, Et0/3
100  Clients                          active    
200  Management                       active    
999  ParkingLot                       active    
1000 Native                           active    
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup 
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 
```

Сконфигурируем и активируем интерфейс управления на S1 (VLAN 200), используя второй IP-адрес из подсети, вычисленный ранее. 

```
S1(config)#int vlan 200  
S1(config-if)#ip address 192.168.1.66 255.255.255.224
S1(config-if)#no shutdown 
S1(config-if)#exit
S1(config)#ip route 0.0.0.0 0.0.0.0 192.168.1.65
```

Настройка для S2:

``` 
S2(config)#int vlan 1
S2(config-if)#ip address 192.168.1.98 255.255.255.240
S2(config-if)#exit
S2(config)#ip route 0.0.0.0 0.0.0.0 192.168.1.97
```

Далее назначены все неиспользуемые порты на S1 VLAN Parking_Lot, и сконфигурированы для статического режима доступа. На S2 административно деактивированы все неиспользуемые порты с помощью команды `interface range`.

```
S1(config)#int range et0/1-2
S1(config-if-range)#switchport mode access 
S1(config-if-range)#switchport access vlan 999
S1(config-if-range)#shutdown 
```
Аналогично: 
``` 
S2(config)#int range et0/1-2
S2(config-if-range)#switchport mode access 
```

Далее назначены VLAN  интерфейсам коммутатора.

S1(config)#int et0/0
S1(config-if)#switchport mode access 
S1(config-if)#switchport access vlan 100
S1(config-if)#no shutdown

``` 
S1(config-if)#do show vlan bri

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et0/3
100  Clients                          active    Et0/0
200  Management                       active    
999  ParkingLot                       active    Et0/1, Et0/2
1000 Native                           active    
1002 fddi-default                     act/unsup 
1003 token-ring-default               act/unsup 
1004 fddinet-default                  act/unsup 
1005 trnet-default                    act/unsup 

```
Порт Et0/3 находится в VLAN по умолчанию и не настроен как магистраль 802.1Q.

Далее необходимо вручную сконфигурировать интерфейс S1 Et0/3 как магистральный 802.1Q.
Изменим режим порта коммутатора на интерфейсе для принудительного подключения к магистрали.

``` 
S1(config)#int et0/3
S1(config-if)#switchport trunk encapsulation dot1q 
```
В рамках конфигурации магистрали установите для собственной VLAN значение 1000.В качестве другой части конфигурации магистрали укажите, что сетям VLAN 100, 200 и 1000 разрешено пересекать магистраль.

```
S1(config-if)#switchport mode trunk 
S1(config-if)#switchport trunk native vlan 1000
S1(config-if)#switchport trunk allowed vlan 1000,100,200
```
Проверим состояние:

``` 
S1#show interfaces trunk 

Port        Mode             Encapsulation  Status        Native vlan
Et0/3       on               802.1q         trunking      1000

Port        Vlans allowed on trunk
Et0/3       100,200,1000

Port        Vlans allowed and active in management domain
Et0/3       100,200,1000

Port        Vlans in spanning tree forwarding state and not pruned
Et0/3       100,200,1000

```
Так как ПК не был подключен к сети с помощью DHCP, IP адреса будут самостоятельно настраивать адрес автоматического частного IP-адреса (APIPA) в диапазоне 169.254.x.x.

#### 2. Настройка и проверка двух серверов DHCPv4 на маршрутизаторе R1.

Для настройки необходимо исключить первые пять доступных адресов из каждого пула адресов.
``` 
R1(config)#ip dhcp excluded-address 192.168.1.1 192.168.1.5
```
Далее создать пул DHCP  и указать сеть, которую поддерживает данный DHCP-сервер.

``` 
R1(config)#ip dhcp pool dhcp_for_R1
R1(dhcp-config)#network 192.168.1.0 255.255.255.192
R1(dhcp-config)#domain-name ccna-lab.com
```

Далее настроим соответствующий шлюз по умолчанию для каждого пула DHCP и время арненды:

```
R1(dhcp-config)#default-router 192.168.1.1
R1(dhcp-config)#lease 2 12 30
```
Аналогично был сконфигурирован второй пул.
``` 
R1(config)#ip dhcp excluded-address 192.168.1.97 192.168.1.101
R1(config)#ip dhcp pool R2_Client_LAN                         
R1(dhcp-config)#network 192.168.1.96 255.255.255.240
R1(dhcp-config)#default-router 192.168.1.97
R1(dhcp-config)#domain-name ccna-lab.com
R1(dhcp-config)#lease 2 12 30

```
Выполним проверка с помощью команды `show ip dhcp pool``, чтобы просмотреть сведения о пуле. 
```
R1#show ip dhcp pool

Pool dhcp_for_R1 :
 Utilization mark (high/low)    : 100 / 0
 Subnet size (first/next)       : 0 / 0 
 Total addresses                : 62
 Leased addresses               : 0
 Pending event                  : none
 1 subnet is currently in the pool :
 Current index        IP address range                    Leased addresses
 192.168.1.1          192.168.1.1      - 192.168.1.62      0

Pool dhcp_for_R2 :
 Utilization mark (high/low)    : 100 / 0
 Subnet size (first/next)       : 0 / 0 
 Total addresses                : 14
 Leased addresses               : 0
 Pending event                  : none
 1 subnet is currently in the pool :
 Current index        IP address range                    Leased addresses
 192.168.1.97         192.168.1.97     - 192.168.1.110     0

 ``` 

Выполним команду `show ip dhcp bindings``, чтобы проверить установленные назначения DHCP-адресов.
``` 
R1#show ip dhcp binding 
Bindings from all pools not associated with VRF:
IP address          Client-ID/     Lease expiration        Type
    Hardware address/
    User name
```
Просмотр статистики:

```  
R1#show ip dhcp server statistics 
Memory usage         25171
Address pools        2
Database agents      0
Automatic bindings   0
Manual bindings      0
Expired bindings     0
Malformed messages   0
Secure arp entries   0

Message              Received
BOOTREQUEST          0
DHCPDISCOVER         0
DHCPREQUEST          0
DHCPDECLINE          0
DHCPRELEASE          0
DHCPINFORM           0

Message              Sent
BOOTREPLY            0
DHCPOFFER            0
DHCPACK              0
DHCPNAK              0
```
Далее проверим корректность настройки и получим адрес для РС.

```
VPCS> ip dhcp  
DDORA IP 192.168.1.6/26 GW 192.168.1.1
```

Просмотр:
``` 
VPCS> show ip

NAME        : VPCS[1]
IP/MASK     : 192.168.1.6/26
GATEWAY     : 192.168.1.1
DNS         : 
DHCP SERVER : 192.168.1.1
DHCP LEASE  : 217795, 217800/108900/190575
DOMAIN NAME : ccna-lab.com
MAC         : 00:50:79:66:68:01
LPORT       : 20000
RHOST:PORT  : 127.0.0.1:30000
MTU         : 1500
```
Проверим подключение, проверив IP-адрес интерфейса Et0/1 R1.

``` 
VPCS> ping 192.168.1.1

84 bytes from 192.168.1.1 icmp_seq=1 ttl=255 time=0.981 ms
84 bytes from 192.168.1.1 icmp_seq=2 ttl=255 time=0.583 ms
84 bytes from 192.168.1.1 icmp_seq=3 ttl=255 time=0.644 ms
84 bytes from 192.168.1.1 icmp_seq=4 ttl=255 time=0.659 ms
84 bytes from 192.168.1.1 icmp_seq=5 ttl=255 time=0.712 ms

```
#### 3. Настройка и проверка DHCP на маршрутизаторе R2.

В части 3 необходимо настроить маршрутизатор R2 для ретрансляции DHCP-запросов из локальной сети по интерфейсу G0/0/1 на DHCP-сервер (R1).

Используем команду `ip helper-address`` на Et0/3, указав IP-адрес R1 Et0/0.
```
R2(config-if)#ip helper-address 10.0.0.1 
```
Далее получаем адрес:

``` 
VPCS> ip dhcp
DDORA IP 192.168.1.101/28 GW 192.168.1.97
```
Как только процесс обновления будет завершен, выполним команду `ipconfig`` для просмотра новой информации об IP.
``` 
VPCS> show ip

NAME        : VPCS[1]
IP/MASK     : 192.168.1.101/28
GATEWAY     : 192.168.1.97
DNS         : 
DHCP SERVER : 10.0.0.1
DHCP LEASE  : 217796, 217800/108900/190575
DOMAIN NAME : ccna-lab.com
MAC         : 00:50:79:66:68:02
LPORT       : 20000
RHOST:PORT  : 127.0.0.1:30000
MTU         : 1500
```
Проверим подключение, проверив IP-адрес интерфейса Et0/1 R1.

```
VPCS> ping 192.168.1.97

84 bytes from 192.168.1.97 icmp_seq=1 ttl=255 time=0.473 ms
84 bytes from 192.168.1.97 icmp_seq=2 ttl=255 time=0.598 ms
84 bytes from 192.168.1.97 icmp_seq=3 ttl=255 time=0.657 ms
84 bytes from 192.168.1.97 icmp_seq=4 ttl=255 time=0.917 ms
84 bytes from 192.168.1.97 icmp_seq=5 ttl=255 time=0.755 ms
```
Выполним команду `show ip dhcp binding`` на R1 для проверки привязок DHCP.
```
R1#show ip dhcp binding
Bindings from all pools not associated with VRF:
IP address          Client-ID/     Lease expiration        Type
    Hardware address/
    User name
192.168.1.6         0100.5079.6668.01       Dec 25 2023 03:05 AM    Automatic
192.168.1.101       0100.5079.6668.02       Dec 25 2023 03:15 AM    Automatic
```
Просмотр статистики:

``` 
R1#show ip dhcp server statistics
Memory usage         41987
Address pools        2
Database agents      0
Automatic bindings   2
Manual bindings      0
Expired bindings     0
Malformed messages   0
Secure arp entries   0

Message              Received
BOOTREQUEST          0
DHCPDISCOVER         4
DHCPREQUEST          2
DHCPDECLINE          0
DHCPRELEASE          0
DHCPINFORM           0

Message              Sent
BOOTREPLY            0
DHCPOFFER            2
DHCPACK              2
DHCPNAK              0

R2#show ip dhcp server statistics 
Memory usage         22563
Address pools        0
Database agents      0
Automatic bindings   0
Manual bindings      0
Expired bindings     0
Malformed messages   0
Secure arp entries   0

Message              Received
BOOTREQUEST          0
DHCPDISCOVER         0
DHCPREQUEST          0
DHCPDECLINE          0
DHCPRELEASE          0
DHCPINFORM           0

Message              Sent
BOOTREPLY            0
DHCPOFFER            0
DHCPACK              0
DHCPNAK              0
```