## Домашнее задание 6
### OSPF

### Цель:
1. Настроить OSPF офисе Москва
2. Разделить сеть на зоны
3. Настроить фильтрацию между зонами

### Описание/Пошаговая инструкция выполнения домашнего задания:

- Маршрутизаторы R14-R15 находятся в зоне 0 - backbone.
- Маршрутизаторы R12-R13 находятся в зоне 10. Дополнительно к маршрутам должны получать маршрут по умолчанию.
- Маршрутизатор R19 находится в зоне 101 и получает только маршрут по умолчанию.
- Маршрутизатор R20 находится в зоне 102 и получает все маршруты, кроме маршрутов до сетей зоны 101.
- Настройка для IPv6 повторяет логику IPv4.

|OSPF|	Area |	Type Area|	Router ID|
|----|-------|-----------|-----------|
|R12|	10|	Stub	|10.2.2.12|
|R13|	10|	Stub	|10.2.2.13|
|R14|	0	|Backbone	|10.2.2.14|
|R14|	10|	Stub	|10.2.2.14|
|R14|	101|	TotalStub	|10.2.2.14|
|R15|	0|	Backbone	|10.2.2.15|
|R15|	10|	Stub	|10.2.2.15|
|R15|	101|TotalStub	|10.2.2.15|
|R19|	101|TotalStub	|10.2.2.19|
|R20|	102|	Standard	|10.2.2.20|

### Маршрутизаторы R14-R15 находятся в зоне 0 - backbone.

Для выполнения настройки были добавлены Loopback адреса по принципу 10.20.1.Х, где `Х` вторая цифра номера устройства для ipv4 адресов, и для ipv6 2001:0DB8:2001:1001::X, где `Х` номер устройства. Подробнее в таблице:

| Device | IPv4 Address | Subnet Mask     | IPv6 GLA Address           | IPv6 LLA Address  |
|--------|--------------|-----------------|----------------------------|-------------------|
| R19    | 10.20.1.9     | 255.255.255.240 | 2001:0DB8:2001:1001::19/64 | FE80::19/8        |   
| R14    | 10.20.1.4     | 255.255.255.240 | 2001:0DB8:2001:1001::14/64 | FE80::14/8        |   
| R12    | 10.20.1.2     | 255.255.255.240 | 2001:0DB8:2001:1001::12/64 | FE80::12/8        |   
| R15    | 10.20.1.5     | 255.255.255.240 | 2001:0DB8:2001:1001::15/64 | FE80::15/8        |   
| R13    | 10.20.1.3     | 255.255.255.240 | 2001:0DB8:2001:1001::13/64 | FE80::13/8        |   
| R20    | 10.20.1.10    | 255.255.255.240 | 2001:0DB8:2001:1001::20/64 | FE80::20/8        |   


Например, для устройства R14:
``` 
R14(config)#interface loopback 1
R14(config)#ip address 10.20.1.4 255.255.255.240
R14(config)#ip ospf network point-to-point	
```
Также настройки включают ipv6 адресацию (по аналогичным настройкам), подробнее в файле конфигурации.

Далее включен OSPF в соответсвии с таблицей и заданы Router-ID:

```
R14(config)#router ospf 1
R14(config-router)#router-id 10.2.2.14
``` 
Включен OSPF непосредственно на интерфейсах между устройствами:

``` 
R15(config)#int et1/0
R15(config-if)#ip ospf 1 area 0
```
Аналогично для ipv6 `ipv6 ospf 1 area 0`

### Маршрутизаторы R12-R13 находятся в зоне 10. Дополнительно к маршрутам должны получать маршрут по умолчанию.

Настройки производились аналогично тем, что указаны в первом пункте задания. В дополнение настроена зона 10 вида stub для выполнения условий задания.

На устройствах R12-R15 выполнено:

``` 
R12(config)#router ospf 10
R12(config-router)#area 10 stub 
```

Включаем на соответствующих интерфейсах маршрутизаторов `ip ospf 1 area 0`.

Для проверки используем команду `show ip route ospf` и `show ipv6 route ospf` на маршрутизаторе R12:

``` 
R12#sh ip route ospf
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is 10.20.2.10 to network 0.0.0.0

O*IA  0.0.0.0/0 [110/11] via 10.20.2.10, 00:16:14, Ethernet0/3
                [110/11] via 10.20.2.21, 00:20:07, Ethernet0/2
      10.0.0.0/8 is variably subnetted, 17 subnets, 4 masks
O IA     10.20.2.4/30 [110/20] via 10.20.2.10, 00:14:19, Ethernet0/3
O        10.20.2.12/30 [110/20] via 10.20.2.38, 00:07:20, Ethernet1/0
                      [110/20] via 10.20.2.10, 00:07:30, Ethernet0/3
O IA     10.20.2.16/30 [110/20] via 10.20.2.21, 00:20:07, Ethernet0/2
O        10.20.2.28/30 [110/20] via 10.20.2.38, 00:07:20, Ethernet1/0
                      [110/20] via 10.20.2.21, 00:07:30, Ethernet0/2
O IA     10.20.2.32/30 [110/20] via 10.20.2.10, 00:14:16, Ethernet0/3
```
```
R12#sh ipv6 route ospf
IPv6 Routing Table - default - 15 entries
Codes: C - Connected, L - Local, S - Static, U - Per-user Static route
       B - BGP, HA - Home Agent, MR - Mobile Router, R - RIP
       H - NHRP, I1 - ISIS L1, I2 - ISIS L2, IA - ISIS interarea
       IS - ISIS summary, D - EIGRP, EX - EIGRP external, NM - NEMO
       ND - ND Default, NDp - ND Prefix, DCE - Destination, NDr - Redirect
       O - OSPF Intra, OI - OSPF Inter, OE1 - OSPF ext 1, OE2 - OSPF ext 2
       ON1 - OSPF NSSA ext 1, ON2 - OSPF NSSA ext 2, la - LISP alt
       lr - LISP site-registrations, ld - LISP dyn-eid, a - Application
OI  ::/0 [110/11]
     via FE80::14, Ethernet0/2
     via FE80::15, Ethernet0/3
O   2001:DB8:1000:1314::/64 [110/20] 
     via FE80::13, Ethernet1/0
     via FE80::14, Ethernet0/2
O   2001:DB8:1000:1315::/64 [110/20] 
     via FE80::15, Ethernet0/3
     via FE80::13, Ethernet1/0
OI  22001:DB8:1000:1415::/64 [110/20] 
     via FE80::14, Ethernet0/2
     via FE80::15, Ethernet0/3   
OI  2001:DB8:1000:1914::/64 [110/20] 
     via FE80::14, Ethernet0/2          
OI  2001:DB8:1000:2015::/64 [110/20] 
     via FE80::15, Ethernet0/3
``` 
 Видно, что маршрутизатор получает маршруты и маршрут по умолчанию посредством OSPF (аналогично на R13).

### Маршрутизатор R19 находится в зоне 101 и получает только маршрут по умолчанию.

Также произведены настройки устройства R19, и определена 101 зона типа totally stub на маршрутизаторах R19,R14:

Создадим зону 101 типа totally stub на маршрутизаторах R19,R14 командой `area 101 stub no-summary`.
Далее на соответствующих интерфейсах маршрутизаторов  был включен `ip ospf 1 area 101`

Для проверки используем команду `show ip route ospf` и `show ipv6 route ospf` на маршрутизаторе R19:

```
R19#sh ip route ospf
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is 10.20.2.18 to network 0.0.0.0

O*IA  0.0.0.0/0 [110/11] via 10.20.2.18, 00:15:23, Ethernet0/0
```
``` 
R19#sh ipv6 route ospf
IPv6 Routing Table - default - 6 entries
Codes: C - Connected, L - Local, S - Static, U - Per-user Static route
       B - BGP, HA - Home Agent, MR - Mobile Router, R - RIP
       H - NHRP, I1 - ISIS L1, I2 - ISIS L2, IA - ISIS interarea
       IS - ISIS summary, D - EIGRP, EX - EIGRP external, NM - NEMO
       ND - ND Default, NDp - ND Prefix, DCE - Destination, NDr - Redirect
       O - OSPF Intra, OI - OSPF Inter, OE1 - OSPF ext 1, OE2 - OSPF ext 2
       ON1 - OSPF NSSA ext 1, ON2 - OSPF NSSA ext 2, la - LISP alt
       lr - LISP site-registrations, ld - LISP dyn-eid, a - Application
OI  ::/0 [110/11]
     via FE80::14, Ethernet0/0
```     
По выводу команды можно отметить, что маршрутизатор получает только маршрут по умолчанию посредством OSPF.

### Маршрутизатор R20 находится в зоне 102 и получает все маршруты, кроме маршрутов до сетей зоны 101.

Аналогично была настроена фильтрация маршрутов для зоны 102 (standart area) в соответствии с заданием.

Включены на соответствующих интерфейсах маршрутизаторов R15 и R20 `ip ospf 1 area 102`

Далее созданы prefix-листы для блокировки на вход в 102 area, а также разрешен остальной трафик, что бы анонс всех маршрутов не заблокировался:

``` 
R15(config)#ip prefix-list block-101 seq 5 deny 10.20.2.16/30          
R15(config)#ip prefix-list block-101  seq 10 permit 0.0.0.0/0 le 32
```
Аналогично для ipv6:

``` 
ipv6 prefix-list area101v6 seq 5 deny 2001:DB8:1000:2015::/64
ipv6 prefix-list area101v6 seq 10 permit ::/0 le 128
```
Применяем созданные prefix-list к зоне 102:

``` 
IPv4
area 102 filter-list prefix area101 in
IPv6

address-family ipv6 unicast
  area 102 filter-list prefix area101v6 in
 exit-address-family
```

Выполняем проверку на маршрутизаторе R20:

```
R20#sh ip route ospf
Codes: L - local, C - connected, S - static, R - RIP, M - mobile, B - BGP
       D - EIGRP, EX - EIGRP external, O - OSPF, IA - OSPF inter area
       N1 - OSPF NSSA external type 1, N2 - OSPF NSSA external type 2
       E1 - OSPF external type 1, E2 - OSPF external type 2
       i - IS-IS, su - IS-IS summary, L1 - IS-IS level-1, L2 - IS-IS level-2
       ia - IS-IS inter area, * - candidate default, U - per-user static route
       o - ODR, P - periodic downloaded static route, H - NHRP, l - LISP
       a - application route
       + - replicated route, % - next hop override

Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 10 subnets, 3 masks
O IA     10.20.2.8/30 [110/30] via 10.20.2.5, 00:09:51, Ethernet0/0
O IA     10.20.2.12/30 [110/30] via 10.20.2.5, 00:09:51, Ethernet0/0
O IA     10.20.2.20/30 [110/20] via 10.20.2.5, 00:09:58, Ethernet0/0
O IA     10.20.2.28/30 [110/20] via 10.20.2.5, 00:09:58, Ethernet0/0
O IA     10.20.2.32/30 [110/20] via 10.20.2.5, 00:09:58, Ethernet0/0
O IA     10.20.2.36/30 [110/30] via 10.20.2.5, 00:09:51, Ethernet0/0
``` 
``` 
R20#sh ipv6 route ospf
IPv6 Routing Table - default - 11 entries
Codes: C - Connected, L - Local, S - Static, U - Per-user Static route
       B - BGP, HA - Home Agent, MR - Mobile Router, R - RIP
       H - NHRP, I1 - ISIS L1, I2 - ISIS L2, IA - ISIS interarea
       IS - ISIS summary, D - EIGRP, EX - EIGRP external, NM - NEMO
       ND - ND Default, NDp - ND Prefix, DCE - Destination, NDr - Redirect
       O - OSPF Intra, OI - OSPF Inter, OE1 - OSPF ext 1, OE2 - OSPF ext 2
       ON1 - OSPF NSSA ext 1, ON2 - OSPF NSSA ext 2, la - LISP alt
       lr - LISP site-registrations, ld - LISP dyn-eid, a - Application
OI  2001:DB8:1000:1213::/64 [110/30]
     via FE80::15, Ethernet0/0
OI  2001:DB8:1000:1214::/64 [110/30]
     via FE80::15, Ethernet0/0
OI  2001:DB8:1000:1215::/64 [110/20]
     via FE80::15, Ethernet0/0
OI  2001:DB8:1000:1314::/64 [110/20]
     via FE80::15, Ethernet0/0
OI  2001:DB8:1000:1315::/64 [110/20]
     via FE80::15, Ethernet0/0
OI  2001:DB8:1000:1415::/64 [110/30]
     via FE80::15, Ethernet0/0

```     
Как видно из результатов пропали маршруты до сетей указанных в prefix-list.

